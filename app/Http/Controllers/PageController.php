<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
        return view('welcome');
    }
    public function home()
    {
        return view('home');
    }
    public function launching()
    {
        return view('launching');
    }
    public function webinar()
    {
        return view('webinar');
    }
    public function exhibit()
    {
        return view('exhibit');
    }
    public function kinabukasan()
    {
        return view('kinabukasan');
    }
    public function kaayusan()
    {
        return view('kaayusan');
    }
    public function kabuhayan()
    {
        return view('kabuhayan');
    }
    public function kalusugan()
    {
        return view('kalusugan');
    }
    public function cest()
    {
        return view('cest');
    }
    public function setup()
    {
        return view('setup');
    }
    public function innovation()
    {
        return view('innovation');
    }
    public function rstl()
    {
        return view('rstl');
    }
    public function schoolar()
    {
        return view('schoolar');
    }
}
