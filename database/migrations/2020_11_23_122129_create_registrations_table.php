<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrations', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('first_name', 50)->nullable();
            $table->string('last_name', 50)->nullable();
            $table->string('middle_name', 50)->nullable();
            $table->string('email', 150)->nullable();
            $table->smallInteger('age')->default(0);
            $table->string('institution', 150)->nullable();
            $table->enum('sex', ['Male', 'Female', null])->nullable();
            $table->string('designation', 100)->nullable();
            $table->string('contact', 100)->nullable();
            $table->enum('sectoral_affiliation', ['PWD', 'IP', 'NONE'])->nullable();
            // age sex designation contact email sectoral affiliation      
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrations');
    }
}
