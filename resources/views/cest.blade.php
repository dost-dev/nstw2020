@extends('layouts.home')
@section('content')
<link href="/css/exhibit.css" rel="stylesheet">

<div class="container text-center border">
    <div class="hello_world " id="images">
    <a href="setup#apo" > <img id="" class="img-fluid" alt="Setup"   src="img/icons/setup_icon.jpg" ></a>
        <a href="rstl#apo" ><img id="" class="img-fluid" alt="RSTL"  src="img/icons/rstl_icon.jpg" ></a>
        <a href="cest#apo" ><img id="" class="img-fluid" alt="cest"  src="img/icons/cest_icon.jpg" ></a>
        <a href="innovation#apo" ><img id="" class="img-fluid" alt="Innovation"  src="img/icons/innovation_icon.jpg" ></a>
        <a href="schoolar#apo" ><img id="" class="img-fluid" alt="Scholarship"   src="img/icons/scholars_icon.jpg" ></a>
    </div>
</div>

  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
  
    <ol class="carousel-indicators" id="">
      
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src=""  style="width:100%;">

        <script>
            function thumbChange(num){
                var thumb = 'img/vid/cest/back' + num + '.png';
                document.getElementById("mainImg").src = thumb;
            }

        </script>
        <div  id="apo" class="container" >

            <div class="main" style="background-color:black; color: white;">
            <br><br>
            <iframe width="540" height="315" src="https://www.youtube.com/embed/wd6DH3KarOc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
                    <div class="u-container-layout u-container-layout-2">
                        <br>
                      <h2 class="text1" style="font-size: 2vw;">
                    Indigenous Peoples Community Empowerment through Science and Technology in Manurigao
                    <br>
                      </h2>
                      <p class="text">
                        <font style="font-size: .9vw;">Barangay Manurigao, New Bataan is one of the identified Geographically Isolated and Disadvantaged Areas (GIDA) in Davao de Oro and listed under the Certificate of Ancestral Domain Title (CADT) area with 100% of the population comprised of Mandaya Indigenous People. This barangay was considered as one of the “red” areas in New Bataan and used to be a hub and training ground of the CNN (CPP-NPA-NDF). 
                          Geographically, it is a barangay surrounded by valleys and composed of 95.71% or 9,208.3100 hectares of forest area. In 2015 PSA census, the barangay has a population of 1,728 people with 327 households. Due to its location and insurgency problems, the whole barangay has a hard time addressing their needs and sustaining their resources and economic subsistence. Brgy. Manurigao lacks access to even the most basic utilities such as Power, Water, Transportation and Communication.
                       </font>
                        <span id="dots">...</span><span id="more">
                        <font  style="font-size: .9vw;">From the data gathered, the marginalized indigenous people who lived in these communities suffered from poverty compounded with the lack of accessibility to basic services such as health care, education, and shelter. Local Government Units and different agencies from National Government also expressed their dilemma on how to put up sustainable livelihood programs and address socio-economic needs in Brgy. Manurigao due to lack of accessibility of road access and communication.
                        Through the collaboration of the Philippine Council for Energy and Emerging Technology Research and Development (DOST-PCIEERD), in partnership with Ateneo de Davao University and LEADTECH Incorporated, Brgy. Manurigao finally got access to electricity for the first time through Micro-Grid Solar PV System which led to an increase in productivity for the livelihood and education of the residents. 
                        This was also followed by the creation of the  Manurigao Development Management Team (MDMT) through the Provincial Executive Order No. 001 series of  2019, by Hon. Jayvee Tyron L. Uy, Governor of Davao de Oro, which aimed to have a team of inter-agency convergence in response to Executive Order 70 of the President of the Philippines, to bring 
                        socio-economic development to a geographically isolated and disadvantaged area besets with insurgency problem. The Manurigao Development Management Team (MDMT) is a representation of the pentahelix model, integrating media in the circle of harmonization between the government, academe, civil society and industry. This led to a more efficient, synchronized and smooth implementation of CEST Manurigao Project.
                        To further address the needs of the community, the Manurigao Development Management Team (MDMT) formulated a Manurigao Strategic Roadmap for 2020-2035 to serve as framework for the delivery of the basic socio-economic services, priority economic infrastructure and utilities towards a rationalized growth and development of barangay Manurigao, New Bataan.
                        Through the continuous coordination of the national and local agencies along with the community leaders and tribal leaders, programs and projects were implemented to Brgy. Manurigao. Similarly, the Department of Science and Technology’s (DOST) Community Empowerment through Science and Technology (CEST) Project entitled, “Indigenous Peoples Community Empowerment through Science and Technology in Manurigao” was implemented with an objective to extend technological assistance thru provision of technology/equipment that will stimulate economic activity, provide livelihood for residents, conduct technology trainings and consultancy services to develop skills for the residents to be more productive, provide DOST FNRI-developed fortified food products that will help alleviate gaps on health and malnutrition and also to install facility that will increase readiness to climate change adaptation.
                        The implementation of the project was initiated by DOST XI in coordination with the beneficiary community, Brgy. Manurigao. The community, with the blessing of the tribal leaders openly embraced the project as a chance to develop their community and has willingly engaged itself to the technicalities and complexities of the said project.  Moreover, CEST Manurigao also opened opportunity to embrace the Mandaya culture of weaving or the “Dagmay” weaving which only 3 traditional Mandaya weavers remain. They also showed progress and own innovations through technologies the DOST XI provided that enhances their way of living. 
                        Continuously, Brgy. Manurigao has progressed and innovated more opportunities through the project. It created a sustainable resource that could provide the community the sustenance they need. Undeniable improvement was observed from the community’s condition and is still progressing through the ingenuity of the community.

                        </font>  </span>
                          <br><br>
                          <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore">Read More</h5>
                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                          <script type="text/javascript">
                            $(document).ready(function(){
                              $('#readMore').click(function(){
                                if($(this).html() == 'Read More'){
                                  $('#dots').hide();
                                  $('#more').show();
                                  $(this).html('Read Less');
  
                                }else {
                                  $('#dots').show();
                                  $('#more').hide();
                                  $(this).html('Read More');
                                }
                                });
                            });
                          </script>
                      </p>
                    </div>
                  </div>
            </div>
            <ul class="thumbs">
              
                
               

            </ul>

        </div>




      </div>

      

       
    </div>
    

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
@endsection



