@extends('layouts.home')
@section('content')
<link href="/css/exhibit.css" rel="stylesheet">
<div class="container text-center border">
<div class="hello_world " id="images">
<a href="setup#apo" > <img id="" class="img-fluid" alt="Setup"   src="img/icons/setup_icon.jpg" ></a>
        <a href="rstl#apo" ><img id="" class="img-fluid" alt="RSTL"  src="img/icons/rstl_icon.jpg" ></a>
        <a href="cest#apo" ><img id="" class="img-fluid" alt="cest"  src="img/icons/cest_icon.jpg" ></a>
        <a href="innovation#apo" ><img id="" class="img-fluid" alt="Innovation"  src="img/icons/innovation_icon.jpg" ></a>
        <a href="schoolar#apo" ><img id="" class="img-fluid" alt="Scholarship"   src="img/icons/scholars_icon.jpg" ></a>
   
    </div>

</div>


  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
  
    <ol class="carousel-indicators" id="">
    
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
      <li data-target="#myCarousel" data-slide-to="4"></li>
      <li data-target="#myCarousel" data-slide-to="5"></li>
      <li data-target="#myCarousel" data-slide-to="6"></li>
      <li data-target="#myCarousel" data-slide-to="7"></li>
      <li data-target="#myCarousel" data-slide-to="8"></li>
      <li data-target="#myCarousel" data-slide-to="9"></li>
      <li data-target="#myCarousel" data-slide-to="10"></li>
      <li data-target="#myCarousel" data-slide-to="11"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src=""  style="width:100%;">

        <script>
            function thumbChange(num){
                var thumb = 'img/exhibit/setup/Apo/back' + num + '.png';
                document.getElementById("mainImg").src = thumb;
            }

        </script>
        <div  id="apo" class="container" >

            <div class="main" style="background-color:black; color: white;">
                <img id="mainImg" src="img/exhibit/setup/Apo/back1.png" >
                <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
                    <div class="u-container-layout u-container-layout-2">
                        <br>
                      <h2 class="text1" style="font-size: 2vw;">Apo ni Lola Durian Delicacies<br>
                      </h2>
                      <p class="text">
                        <font style="font-size: .9vw;">Had its humble beginnings as a microenterprise owned by Mr. Arnel Raakin, the grandchild of the famous durian candy maker, Abondia del Puerto Raakin or Lola Abon. Driven by the desire to build his own business, Mr. Raakin innovated their heirloom recipe of pastillas candies to soft consistency yema candies. Then with many years of hard work and perseverance and by surrounding himself with the right people, what began as a small store has now become a popular Davao City pasalubong destination.
                        Apo ni Lola’s main business strategy is constant product innovation to come up with a wide array of products to offer its customers. Currently, it has five (5) major product lines: a) candies, b) baked products, c) ice cream, d) smoothies/shakes, and e) durian flavored coffee. In addition to durian-based treats, it also manufactures products made from other local produce such as mangosteen, jackfruit, marang, mango and coffee. With the DOST-SETUP interventions, Apo ni Lola increased its production capacity and expanded its product line, especially the baked goods.          </font>
                        <span id="dots">...</span><span id="more">
                        <font  style="font-size: .9vw;">It significantly improved its productivity which translated into increase in gross sales when compared to the baseline data. Also, the company generated additional employment. 
                        Now, with the effects of the COVID-19 pandemic in the tourism industry, Apo ni Lola has since directed its attention in serving the local market. It has repositioned itself not only as a source of must-have pasalubong items for tourists but also as comfort food Dabawenyos can enjoy. It has expanded the coverage of its in-house delivery service and is taking the necessary steps to negotiate with other food delivery service providers. It also strengthened the use of social media platforms to promote its products and interact with customers.      
                        Apo ni Lola was provided financial assistance for the acquisition of the following: 
                        Upright chiller, Planetary mixer, Staff ice gelato machine, Bakery oven, Dough roller, Stainless-steel fixtures, Cooling racks and bakery trays, Coffee machine, Packaging materials
                        
                        It also benefitted from the following training and consultancy services:
                        GMP Awareness Seminar 
                        Manufacturing Productivity Extension Program
                        Consultancy for Agricultural and Manufacturing Productivity Improvement (CAMPI) Program
                        Cleaner Production Technology
                        Packaging and label design
                        </font>  </span>
                          <br><br>
                          <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore">Read More</h5>
                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                          <script type="text/javascript">
                            $(document).ready(function(){
                              $('#readMore').click(function(){
                                if($(this).html() == 'Read More'){
                                  $('#dots').hide();
                                  $('#more').show();
                                  $(this).html('Read Less');
  
                                }else {
                                  $('#dots').show();
                                  $('#more').hide();
                                  $(this).html('Read More');
                                }
                                });
                            });
                          </script>
                      </p>
                    </div>
                  </div>
            </div>
            <ul class="thumbs">
                <li onclick="thumbChange(1)"><img src="img/exhibit/setup/Apo/back1.png"></li>
                <li onclick="thumbChange(2)"><img src="img/exhibit/setup/Apo/back2.png"></li>
                
               

            </ul>

        </div>




      </div>

      <div class="item" >
        <img src=""  style="width:100%;">

                <script>
            function thumbChange1(num){
                var thumb = 'img/exhibit/setup/jo/back' + num + '.png';
                document.getElementById("mainImg2").src = thumb;
            }

        </script>
        <div class="container">

        <div class="main" style="background-color:black; color: white;">
            <img id="mainImg2" src="img/exhibit/setup/jo/back1.png" >
            <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
                    <div class="u-container-layout u-container-layout-2">
                        <br>
                      <h2 class="text1" style="font-size: 2vw;">A Sumptuous Taste of Success: The Story of Jopoy’s Special Hopia<br>
                      </h2>
                      <p class="text">
                        <font style="font-size: .9vw;">They say that a way to a man’s heart is through his stomach. Indeed, with just the right blend of mouth-watering flavor and the enticing aroma of a freshly-baked pastry,  Jopoy’s Special Hopia has conquered the hearts and above all the appetite of Digoseños. Mr. Oscar “Jojo” Caballero envisioned to bring in a unique taste of hopia to Digos City after learning its production processes from his aunt’s business in Taguig City, Metro Manila. Starting from square one, he had only Php 40,000.00 beginning capital to start up his hopia making business in 2008 at their backyard in Hagonoy, Davao del Sur.   The first products were mongo and ube flavored hopia.  To establish its name, Mr. Caballero vended his products house to house on his motorcycle. The business name Jopoy’s stands for the nickname of the owners, Jojo and wife,  Popoy.        </font>
                        <span id="dots">...</span><span id="more2">
                        <font  style="font-size: .9vw;">By 2010, Jopoy’s Special Hopia earned its prominence in Digos City. This has prompted Mr. Caballero to rent a small space in Roxas Extension near UM Digos College to sell its freshly-baked hopia which added the students to the influx of its customers. However, with the limited space, they have to cook the fillings in their old house in Hagonoy while the dough are made in their house in Digos City- these are all delivered to their store for baking.  In 2013, pastillas fillings were added to their mongo and ube flavored hopia. Customers flocked to their store that as early as 12:00 noon each day, all products were already sold out. As Jopoy’s gained wider popularity, several resellers started to come to their store and sold their products in bus terminals and even along the bus stops in the highways.  Jopoy’s Special Hopia became a sought after pasalubong for travelers. 
                        As the demand grew bigger and bigger, the need for a new production and GMP-compliant plant arises. Mr. Caballero sought DOST’s assistance on GMP Assessment and Plant Layout consultancies and laboratory analysis of products in 2015 thus, earning its License to Operate (LTO) from the Food and Drug Administration (FDA) in 2016.  DOST also assisted the firm in providing package/label design, nutritional facts analysis, as well as various technical trainings and consultancy services.  In 2019, the firm was assisted by DOST XI under SETUP for the acquisition of 1 unit Cooker Mixer, 1 unit peanut grinder and 1 unit roaster with a total project fund of PhP 533,500.00. To capture a bigger market share, Jopoy’s Special Hopia diversified its products by coming out with two (2) more variants of hopia - pineapple and durian fillings (2018) and other products such as cookies and peanut butter (2019). With these milestones, Jopoy’s Special Hopia has made its name in the competitive market across municipalities in Davao del Sur, as well as in Davao City, General Santos City and even in Cotabato.  All these escalated the firm’s status from a humble backyard business to a medium-sized enterprise to date.


                    </font>  </span>
                          <br><br>
                          <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore2">Read More</h5>
                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                          <script type="text/javascript">
                            $(document).ready(function(){
                              $('#readMore2').click(function(){
                                if($(this).html() == 'Read More'){
                                  $('#dots').hide();
                                  $('#more2').show();
                                  $(this).html('Read Less');
  
                                }else {
                                  $('#dots').show();
                                  $('#more2').hide();
                                  $(this).html('Read More');
                                }
                                });
                            });
                          </script>
                      </p>
                    </div>
                  </div>
        </div>
            <ul class="thumbs">
            <li onclick="thumbChange1(1)"><img src="img/exhibit/setup/jo/back1.png"></li>
            <li onclick="thumbChange1(2)"><img src="img/exhibit/setup/jo/back2.png"></li>
           
            
        
        
            

            </ul>

            </div>
        </div>
    
      <div class="item">
        <img src=""  style="width:100%;">
        <script>
            function thumbChange2(num){
                var thumb = 'img/exhibit/setup/fab/back' + num + '.png';
                document.getElementById("mainImg3").src = thumb;
            }

        </script>
        <div class="container">

        <div class="main" style="background-color:black; color: white;">
            <img id="mainImg3" src="img/exhibit/setup/fab/back1.png" >
            <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
                    <div class="u-container-layout u-container-layout-2">
                        <br>
                      <h2 class="text1" style="font-size: 2vw;">DP Fabrication and Machineries: From Bottles to Machine<br>
                      </h2>
                      <p class="text">
                        <font style="font-size: .9vw;">DP Fabrication and Machineries is one of the most successful projects of DOST’s Small Enterprise Technology Upgrading Program (SETUP) in the Davao Region.  Owned by Mr. Demetrio C. Perez and co-managed by his children, the firm promotes agricultural mechanization as solution to address sustainable food production and dwindling agricultural workforce through production of affordable and quality agricultural machineries and farm implements. DOST was the first government agency to extend assistance to DP Fabrication and Machineries. In 2014, the firm was able to acquire a Lathe Machine and a Milling Machine amounting to PhP 680,000.00 from DOST’s SETUP. The huge demand from the local market was the primary driver of the firm’s venture which motivates them to increase their production capacity and improve their product quality.     </font>
                        <span id="dots">...</span><span id="more3">
                        <font  style="font-size: .9vw;"> Hence, the firm applied again for SETUP in 2017 amounting to PhP 2,889,100.00 for the acquisition of a CNC Cutting Machine and a CAD Workstation.Through adoption of technology upgrades brought by SETUP, DPFM was able to perform 3-D machine drawings and CNC Cutting. The machine produces precise and quality outputs which significantly reduced the production lead time and improved the firm’s productivity. Aside from the financial assistance, DOST also provided various technical trainings and technical consultancies such as Manufacturing Productivity Extension (MPEX), Cleaner Production Technology (CPT) and Energy Audit.
                        With these technological and technical assistance coupled with hard work and determination, the firm was able to increase their production capacity, improve their product quality as well as upgrade its business status from micro to medium enterprise. From 6 product lines, they could now offer 33 fabrication jobs. Because of its upgrading and additional line of services, employment generated has also significantly increased from 6 to 30 workers including indigenous people (IPs). The firm was also able to expand their market not just in Davao Region but all throughout the country.  Along with these changes comes the improvement on their production facility from a small, disheveled workplace in Digos City, Davao del Sur to a bigger and highly orderly facility in Brgy. Sinaragan, Matanao, Davao del Sur. 
                        With services geared towards their customer’s interest, the firm developed movable designs of basic agricultural machineries like corn shellers, corn mills, and rice mills among others to enable convenient handling especially for small farmers. The mobile machineries could be operated from one place to another and could do on-site processing of produce which is ideal for village-level farmers to save time and effort. It is also diesel-fed which greatly reduce the operating costs for the electricity needed to run the machine and is intended for remote areas with no electricity. For small farmers, these village-type agricultural machineries are affordable, convenient, cost-efficient, and practical to use.
                        In its continuous pursuit to produce globally competitive machine products, the firm invested on a computer program called SolidWorks which is used for planning, visual ideation, modeling, feasibility assessment, and prototyping. The firm is now able to analyze the structural design, motions and flows, and connection details of each product before it endorses for production. 
                        DP Fabrication and Machineries is an active industry partner and commit to support the Agricultural and Fisheries Mechanization (AFMech) Law. All machine products, both prototypes and commercialized machines, are tested and registered with AMTEC (Agricultural Machinery Testing and Evaluation Center). With the AMTEC certification, the customers can assure that machine products are conformed with the Philippine National Standards (PNS). The firm also applied for and acquired agricultural machinery designs from various government agencies (DOST-MIRDC, PHILMECH, and PhilRice) and the Academe (UPLB, UST, etc). This is to support sustainable agricultural mechanization in the Philippines by making locally made agricultural machineries available commercially.
                        DP Fabrication and Machineries was an awardee of the “Best SETUP Adopter” of the Department of Science and Technology (DOST) in 2017. Since its availment of the SETUP in 2014, the firm has been nominated and awarded by other prestigious bodies such as “The Golden Annual Awards for Business Excellence” in 2016, “Business Innovation of the Year Awards” by the Provincial Government of Davao del Sur in 2017, and finalist of the “National Search for Hometown Heroes” in 2017. 
                        </font>  </span>
                          <br><br>
                          <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore3">Read More</h5>
                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                          <script type="text/javascript">
                            $(document).ready(function(){
                              $('#readMore3').click(function(){
                                if($(this).html() == 'Read More'){
                                  $('#dots').hide();
                                  $('#more3').show();
                                  $(this).html('Read Less');
  
                                }else {
                                  $('#dots').show();
                                  $('#more3').hide();
                                  $(this).html('Read More');
                                }
                                });
                            });
                          </script>
                      </p>
                    </div>
                  </div>
           </div>
        <ul class="thumbs">
            <li onclick="thumbChange2(1)"><img src="img/exhibit/setup/fab/back1.png"></li>
            <li onclick="thumbChange2(2)"><img src="img/exhibit/setup/fab/back2.png"></li>
            <li onclick="thumbChange2(3)"><img src="img/exhibit/setup/fab/back3.png"></li>
            <li onclick="thumbChange2(4)"><img src="img/exhibit/setup/fab/back4.png"></li>
            
            
        
    

        </ul>

        </div>
      </div>
      <div class="item">
        <img src=""  style="width:100%;">
        <script>
            function thumbChange3(num){
                var thumb = 'img/exhibit/setup/chain/back' + num + '.png';
                document.getElementById("mainImg4").src = thumb;
            }

        </script>
        <div class="container">

        <div class="main" style="background-color:black; color: white;">
            <img id="mainImg4" src="img/exhibit/setup/chain/back1.png" >
            <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
                    <div class="u-container-layout u-container-layout-2">
                        <br>
                      <h2 class="text1" style="font-size: 2vw;">Establishment of Cold Chain Facility to Prolong the Shelf-life of Fresh Frozen Durian and Reduce Spoilage<br>
                      </h2>
                      <p class="text">
                        <font style="font-size: .9vw;">D’ Farmers Market is a family-owned company established by Mr. Candelario Miculob. It is a single proprietorship enterprise engaged in the production of minimally processed fruits, especially fresh and frozen durian, for retail and wholesale. Mr. Miculob became more passionate with farming after he acquired a 14.3-hectare land in one of the agricultural areas of Davao City. He further focused in growing his business after his early retirement from a corporate job. Seeing the great potential of Davao City’s export quality fruits, Mr. Larry Miculob invested on putting up a fruit processing facility inside his farm. In 2012, a pig pen was converted into a production plant using as guide the plant layout designed by the Davao Food Safety Team (DFST).           </font>
                        <span id="dots">...</span><span id="more4">
                        <font  style="font-size: .9vw;">
                        Seeing the great potential of Davao City’s export quality fruits, Mr. Larry Miculob invested on putting up a fruit processing facility inside his farm.
                        In 2012, a pig pen was converted into a production plant using as guide the plant layout designed by the Davao Food Safety Team (DFST).
                        Good Manufacturing Practices (GMP) Plant Layout Consultancy Service
                        Packaging and label design assistance
                        GMP Awareness Seminar
                        Manufacturing Productivity Extension Program
                        Consultancy for Agricultural and Manufacturing Productivity Improvement (CAMPI)
                        Training on Vacuum Frying
                        Discounted laboratory services – Microbiology and Chemistry labs
                        Durian is a seasonal fruit which is typically ubiquitous starting the month of August. During this time the local market is saturated with produce leading to low prices that may not generate enough return of investment. With the use of an effective cold storage facility, the harvested durian are hygienically stocked to prevent wastage during peak season and make it available whole year round. The same is true with the other tropical fruits which we have abundance. The application of appropriate preservation processes can prolong the shelflife of agricultural produce up to two (2) years under suitable storing conditions.
                        The consultancy services provided to D’ Farmers enabled them to improve in its GMP-compliance, and address operational and management issues. Furthermore, the recommendations from these consultancy experts had been contributory in securing a GMP Certification which allowed the firm to exploit new market opportunities.
                        Moreover, the upgraded equipment and GMP-compliant facility meant greater capacity to cater to the demands of the export market.
                                                                   
                      </font>  </span>
                          <br><br>
                          <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore4">Read More</h5>
                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                          <script type="text/javascript">
                            $(document).ready(function(){
                              $('#readMore4').click(function(){
                                if($(this).html() == 'Read More'){
                                  $('#dots').hide();
                                  $('#more4').show();
                                  $(this).html('Read Less');
  
                                }else {
                                  $('#dots').show();
                                  $('#more4').hide();
                                  $(this).html('Read More');
                                }
                                });
                            });
                          </script>
                      </p>
                    </div>
                  </div>
           </div>
        <ul class="thumbs">
            <li onclick="thumbChange3(1)"><img src="img/exhibit/setup/chain/back1.png"></li>
            <li onclick="thumbChange3(2)"><img src="img/exhibit/setup/chain/back2.png"></li>
            <li onclick="thumbChange3(3)"><img src="img/exhibit/setup/chain/back3.png"></li>
            <li onclick="thumbChange3(4)"><img src="img/exhibit/setup/chain/back4.png"></li>
            
            
        
    

        </ul>

        </div>
      </div>
      <div class="item">
        <img src=""  style="width:100%;">
        <script>
            function thumbChange4(num){
                var thumb = 'img/exhibit/setup/ice/back' + num + '.png';
                document.getElementById("mainImg5").src = thumb;
            }

        </script>
        <div class="container">

        <div class="main" style="background-color:black; color: white;">
            <img id="mainImg5" src="img/exhibit/setup/ice/back1.png" >
            <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
                    <div class="u-container-layout u-container-layout-2">
                        <br>
                      <h2 class="text1" style="font-size: 2vw;">Technology Upgrading of Ice Cream Production<br>
                      </h2>
                      <p class="text">
                        <font style="font-size: .9vw;">Magvilla Food Products is a single proprietorship type of business owned and managed by spouses Mr. and Mrs. Victoriano and Villa Vicada. It holds a business permit from the Local Government Unit of Tagum City with registration number 1806 and the Department of Trade and Industry (DTI) bearing the certificate number 03152839. The enterprise is a member of Food Processors Association of Davao del Norte (FPAD-DN).
                        Magvilla Food Products’ humble beginnings can be traced in year 2001 when Mrs. Vicada saw the potential of ice cream as a business venture. Mr. Vicada then improved the recipes of his wife and produced ice cream for relatives and friends during special occasions to test its acceptability and marketability. After gathering positive feedbacks, he decided to commercialize his product and formally started the business in September 2003.
                        From then on, Mr. Vicada continuously conducts market research and product development   
                      </font>
                        <span id="dots">...</span><span id="more5">
                        <font  style="font-size: .9vw;">
                        in order to offer delicious quality ice cream to the consuming public and be at par with its competitors.
                       In year 2012, Mr. Vicada availed assistance from the Small Enterprise Technology Upgrading Program (SETUP) of the Department of Science and Technology (DOST). Magvilla Food Products was provided with 2-in-1 Ice Cream Maker / Homogenizer, thus, the enterprise was able to increase its productivity. In addition to that, it was also provided with package of assistance which includes GMP training, microbiological and chemical analyses, label design and consultancy services from Manufacturing Productivity Extension (MPEX) Program and Davao Region Energy Audit and Management (DREAM) Team. 
                        With Mr. Vicada’s passion for product and process innovation, he decided to avail financial assistance from DOST XI once again in 2016. His recent equipment upgrading facilitated further diversification of his product line, and improvement in the texture and consistency of his ice cream products. 
                        At present, Magvilla Food Products’s products are already available at the Stall #22B in Trade Center of Tagum City. The firm can now accommodate more demand for ice cream as they are already able to supply to schools, as well as various pasalubong centers located in Citi-Mall, Robinson Center, and Energy Park, Tagum City. They are also able to reach resellers situated in municipalities of Sto. Tomas, Asuncion, Kapalong, Panabo and Nabunturan. Mr. Vicada has started a goal, as of the first quarter of 2019, of adding 15-20 outlets every quarter in the provinces of Davao del Norte and Compostela Valley.
                        Now, the enterprise already has an SSOP Manual and License-To-Operate from the Food and Drug Administration-Philippines. The S&T Interventions provided to the firm have greatly helped it to improve its productivity, increase its market share and eventually, its profitability.  
                        </font>  </span>
                          <br><br>
                          <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore5">Read More</h5>
                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                          <script type="text/javascript">
                            $(document).ready(function(){
                              $('#readMore5').click(function(){
                                if($(this).html() == 'Read More'){
                                  $('#dots').hide();
                                  $('#more5').show();
                                  $(this).html('Read Less');
  
                                }else {
                                  $('#dots').show();
                                  $('#more5').hide();
                                  $(this).html('Read More');
                                }
                                });
                            });
                          </script>
                      </p>
                    </div>
                  </div>
           </div>
        <ul class="thumbs">
            <li onclick="thumbChange4(1)"><img src="img/exhibit/setup/ice/back1.png"></li>
            <li onclick="thumbChange4(2)"><img src="img/exhibit/setup/ice/back2.png"></li>
            <li onclick="thumbChange4(3)"><img src="img/exhibit/setup/ice/back3.png"></li>
            <li onclick="thumbChange4(4)"><img src="img/exhibit/setup/ice/back4.png"></li>
            
            
        
    

        </ul>

        </div>
      </div>
      <div class="item">
        <img src=""  style="width:100%;">
        <script>
            function thumbChange5(num){
                var thumb = 'img/exhibit/setup/vid/back' + num + '.png';
                document.getElementById("mainImg6").src = thumb;
            }

        </script>
        <div class="container">

        <div class="main" style="background-color:black; color: white;">
            <img id="mainImg6" src="img/exhibit/setup/vid/back1.png" >
            <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
                    <div class="u-container-layout u-container-layout-2">
                        <br>
                      <h2 class="text1" style="font-size: 2vw;">Productivity Improvement through Equipment Acquisition for Web and Design Development of AnimateWell Web Graphic Design<br>
                      </h2>
                      <p class="text">
                        <font style="font-size: .9vw;">AnimateWell Web Graphic Design is a micro-scale enterprise owned by Ms. Claudin D. Lanaza. This is located inside Purok Ferraren, Magugpo East, Tagum City. It holds a business permit from the Local Government Unit of Tagum City with registration number 2870 and the Department of Trade and Industry (DTI) bearing the certificate number 04125356.  
                        The enterprise is responsible for creating, editing and designing all types of videos for its clients.
                           </font>
                        <span id="dots">...</span><span id="more6">
                        <font  style="font-size: .9vw;">
                        They work on raw camera footages, video clips, photographs, sound, music, graphic design and special effects which are appropriate to the video concepts.  The owner and his workforce cater to clients abroad through online transactions.
                        With the large potential of foreign market clientele, the enterprise sees the need to purchase additional computer units dedicated for coding and web design. Moreover, the firm has to upgrade the laptops (through replacement) which are used for video editing to desktops. Per observation, updating the peripherals are easier with desktops compared to laptops. Having the said equipment will enable the firm to cater additional customers, gain more income, and generate more employment. 
                        In year 2018, Ms. Claudin Lanaza availed assistance from the Small Enterprise Technology Upgrading Program (SETUP) of the Department of Science and Technology (DOST). AnimateWell Web Graphic Design was provided with six (6) computer units and one (1) server, thus, the enterprise was able to increase its productivity and create more enhanced products. The firm can now accommodate more job orders of web design, graphic design and video editing. In addition to that, it was also provided with consultancy service from Manufacturing Productivity Extension (MPEX) Program. 
                       </font>  </span>
                          <br><br>
                          <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore6">Read More</h5>
                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                          <script type="text/javascript">
                            $(document).ready(function(){
                              $('#readMore6').click(function(){
                                if($(this).html() == 'Read More'){
                                  $('#dots').hide();
                                  $('#more6').show();
                                  $(this).html('Read Less');
  
                                }else {
                                  $('#dots').show();
                                  $('#more6').hide();
                                  $(this).html('Read More');
                                }
                                });
                            });
                          </script>
                      </p>
                    </div>
                  </div>
           </div>
        <ul class="thumbs">
            <li onclick="thumbChange5(1)"><img src="img/exhibit/setup/vid/back1.png"></li>
            <li onclick="thumbChange5(2)"><img src="img/exhibit/setup/vid/back2.png"></li>
            <li onclick="thumbChange5(3)"><img src="img/exhibit/setup/vid/back3.png"></li>
          
        </ul>

        </div>
      </div>
      <div class="item">
        <img src=""  style="width:100%;">
        <script>
            function thumbChange6(num){
                var thumb = 'img/exhibit/Innovation/Starbooks/back' + num + '.png';
                document.getElementById("mainImg7").src = thumb;
            }

        </script>
        <div class="container">

        <div class="main" style="background-color:black; color: white;">
            <img id="mainImg7" src="img/exhibit/Innovation/Starbooks/back1.png" >
            <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
                    <div class="u-container-layout u-container-layout-2">
                        <br>
                        <h2 class="text1" style="font-size: 2vw;">Science and Technology Academic and Research-Based Openly Operated Kiosks (STARBOOKS)<br>
                      </h2>
                      <p class="text">
                        <font style="font-size: .9vw;">The first STARBOOKS and Solar Powered STARBOOKS in the Philippines were installed at Davao Region. STARBOOKS aims to provide learners and learning institutions with a tool to gain knowledge even without internet. In total, DOST XI have deployed 327 STARBOOKS. DOST XI innovated, produced and deployed Solar Powered STARBOOKS to provide learners at Geographically Isolated and Disadvantaged Areas with means to gain knowledge even without internet and even without electricity. Currently, DOST XI have deployed 5 Solar Powered STARBOOKS in the Region.      </font>
                        <span id="dots">...</span><span id="more10">
                        <font  style="font-size: .9vw;">
                        DOST XI puts prime in partnering with different instutitions. Currently, DOST XI has forged partnerships with: DepEd XI, TESDA XI, DTI XI, Therma South Inc. (Aboitiz) and University of Mindanao, LGU Davao City, LGU Digos City and United Nations World Food Programme.
                        </font>  </span>

                          <br><br>
                          <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore7">Read More</h5>
                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                          <script type="text/javascript">
                            $(document).ready(function(){
                              $('#readMore7').click(function(){
                                if($(this).html() == 'Read More'){
                                  $('#dots').hide();
                                  $('#more7').show();
                                  $(this).html('Read Less');
  
                                }else {
                                  $('#dots').show();
                                  $('#more7').hide();
                                  $(this).html('Read More');
                                }
                                });
                            });
                          </script>
                      </p>
                    </div>
                  </div>
           </div>
        <ul class="thumbs">
            <li onclick="thumbChange6(1)"><img src="img/exhibit/Innovation/Starbooks/back1.png"></li>
            <li onclick="thumbChange6(2)"><img src="img/exhibit/Innovation/Starbooks/back2.png"></li>
            <li onclick="thumbChange6(3)"><img src="img/exhibit/Innovation/Starbooks/back3.png"></li>
          
            
            
        
    

        </ul>

        </div>
      </div>
      <div class="item">
        <img src=""  style="width:100%;">
        <script>
            function thumbChange7(num){
                var thumb = 'img/exhibit/halal/back' + num + '.png';
                document.getElementById("mainImg8").src = thumb;
            }

        </script>
        <div class="container">

        <div class="main" style="background-color:black; color: white;">
            <img id="mainImg8" src="img/exhibit/halal/back1.png" >
            <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
                    <div class="u-container-layout u-container-layout-2">
                        <br>
                      <h2 class="text1" style="font-size: 2vw;">NARRATIVE REPORT ON THE ESTABLISHMENT AND OPERATIONALIZATION OF HALAL VERIFICATION LABORATORY (PY 2016-2020)<br>
                      </h2>
                      <p class="text">
                        <font style="font-size: .9vw;">
                        The Department of Science and Technology Region XI - Halal Verification Laboratory, one of the three DOST Halal Verification Laboratories under the DOST Halal S & T Program was established in support to the implementation of the Republic Act 10817 known as the Philippine Halal Export Development and Promotion Act of 2016.The act recognizes the role of exports of Halal industries to the national economic development;                  </font>
                        <span id="dots">...</span><span id="more8">
                        <font  style="font-size: .9vw;">
                        thus declares promotion of the integrity and quality of Philippine Halal Exports.
                        The DOST play a key role in the export and trade regulation of Halal products, processes and services by offering a globally competitive laboratory tests and services relative to research and product development and quality assurance.   
                        The two-year project “Setting-Up of One-Stop Laboratory Services for Global Competitiveness (OneLab): Establishment of Halal Laboratory in Region XI” which embarked on 01 January 2016 is the first DOST Halal project. The project focused on international Halal Laboratory benchmarking activities and trainings, and procurement of the necessary equipment and materials identified at the course of benchmarking for the establishment of Halal Laboratory.
                        Following the establishment of the Halal Verification Laboratory, a three-year project “Operationalization and/or Accreditation of Halal Verification Laboratories (HVLs) of DOST Regions CALABARZON, 11, 12 and ARMM in Support to Halal Assurance System” was implemented.The project is one of the nine projects under the DOST Halal S & T Program headed by Dr. Anthony C. Sales.
                        The establishment and operationalization of the three Halal Verification Laboratories housed in MOST BARMM Complex, Cotabato City, Maguindanao; DOST CALABARZON Complex, Jamboree Road Timugan, Los Baños, Laguna; and DOST Davao, Corner Friendship and Dumanlas Roads, Bajada, Davao City is funded by the DOST Philippine Council for Industry, Energy and Emerging Technology Research and Development.       </font>  </span>
                          <br><br>
                          <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore8">Read More</h5>
                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                          <script type="text/javascript">
                            $(document).ready(function(){
                              $('#readMore8').click(function(){
                                if($(this).html() == 'Read More'){
                                  $('#dots').hide();
                                  $('#more8').show();
                                  $(this).html('Read Less');
  
                                }else {
                                  $('#dots').show();
                                  $('#more8').hide();
                                  $(this).html('Read More');
                                }
                                });
                            });
                          </script>
                      </p>
                    </div>
                  </div>
            </div>
            <ul class="thumbs">
              <li onclick="thumbChange7(1)"><img src="img/exhibit/halal/back1.png"></li>
              <li onclick="thumbChange7(2)"><img src="img/exhibit/halal/back2.png"></li>
              <li onclick="thumbChange7(3)"><img src="img/exhibit/halal/back3.png"></li>
              <li onclick="thumbChange7(4)"><img src="img/exhibit/halal/back4.png"></li>

            </ul>

        </div>
      </div>
    <div class="item">
        <img src=""  style="width:100%;">
        <script>
            function thumbChange8(num){
                var thumb = 'img/exhibit/setup/tree/back' + num + '.png';
                document.getElementById("mainImg9").src = thumb;
            }

        </script>
        <div class="container">
            <div class="main" style="background-color:black; color: white;">
                <img id="mainImg9" src="img/exhibit/setup/tree/back1.png" >
                <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
                      <div class="u-container-layout u-container-layout-2">
                            <br>
                          <h2 class="text1" style="font-size: 2vw;">THE TREE OF BUSINESS SUCCESS<br></h2>
                   
                          <p class="text">
                            <font style="font-size: .9vw;">
                            Farming is his first love.  As for Mr. Benjamin R. Lao, President and CEO of the Lao Integrated Farms, Inc. (LIFI), he gave up his prestigious post and white-collar job at the Bureau of Immigration to toil his 5-hectare coconut plantation at Brgy. Eman, Bansalan, Davao del Sur. His passion for farming breathed life into a barren land and transformed it into a laudable business.
                            One of the main features in the fields of Davao del Sur is the looming abundance of coconut trees. Being one of the top provinces to produce millions of metric tons in the Davao Region, the promising potential of the coconut industry roused the interest of Mr. Lao to produce coconut sugar and coconut syrup.  The business was first known in 2008 as Donna Belle Delicacies having an initial capital of PhP 2,800.00. On the same year, Mr. Lao applied for the Department of Science and Technology-Grants in Aid (DOST-GIA) Program for the acquisition of basic equipment.       </font>
                            <span id="dots">...</span><span id="more9">
                            <font  style="font-size: .9vw;">
                            The increasing product demand and goal to penetrate international markets prompted Mr. Lao to cope up with quality standards and production volume. The business availed the DOST’s Small Enterprises Technology Upgrading Program (SETUP) twice for the upgrading of its production equipment and facilities.  Aside from SETUP, technical consultancies such as Manufacturing Productivity Extension (MPEX), Food Safety Consultancies, Product Packaging, and Energy Audit were also provided to the firm to improve its productivity and product quality. The business earned various certifications such as Hazard Analysis Critical Control Points (HACCP) by TUV SUD Germany, Eco cert France, USDA Organic, Halal, FDA, and Kosher - these certifications enabled the business to capture export markets. These efforts eventually paid off as the results almost tangibly show how high up the business soared as seen through the increase in productivity, sales, and widespread reach of their markets. Products are sold in various local outlets in the various areas in Mindanao, Visayas and National Capital Region. The business export markets include Germany, USA, New Zealand, France, Canada, Japan, United Kingdom and Australia.Mr. Lao conceived the idea that the market research never ends.  It is from this contention that updated products are always borne out from continues product development. Starting with only two (2) product lines, the firm is now into production of other coconut toddy-based variants such as guyabano syrup, coconut syrup with Virgin Coconut Oil (VCO), healthy teas with lemon grass, moringa, turmeric and ginger. The business is also producing home-made ice cream and natural condiments like sweet and spicy seasonings and coconut vinegar. 
                            Government’s intervention did not just inspire the growth of the business, as it was also obvious how the assistance cascaded into changing the lives of the backbone of their operation - the workers. Starting with only five (5) regular workers and two (2) toddy suppliers, the firm was now able to generate rural employment to a total of 357 workforce, most of them are coconut toddy gatherers. One of the advocacies of the business is the preservation of the indigenous culture. Hence, most of their workers and toddy gatherers are composed of the indigenous ethnic group, Tribo Bagobo. The endeavor has uplifted the socio-economic status of coconut toddy tappers. Furthermore, the project generated a relative increase in business’ revenue that boosts the economic activities and development of the community.  
                            With the concept to integrate agriculture and processing of agricultural products, the business name was then changed into Lao Integrated Farms, Inc. (LIFI). LIFI is an advocate of organic agriculture. It has also a demo farm which serves as an agri-tourism site showcasing various sustainable farming technologies. From a rural-based micro enterprise, LIFI operates as medium enterprise to date.           <br><br>
                              <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore9">Read More</h5>
                              <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                              <script type="text/javascript">
                                $(document).ready(function(){
                                  $('#readMore9').click(function(){
                                    if($(this).html() == 'Read More'){
                                      $('#dots').hide();
                                      $('#more9').show();
                                      $(this).html('Read Less');
      
                                    }else {
                                      $('#dots').show();
                                      $('#more9').hide();
                                      $(this).html('Read More');
                                    }
                                    });
                                });
                              </script>
                         </p>
                        
                          </div>
                              </div>
                      </div>
                        <ul class="thumbs">
                        <li onclick="thumbChange8(1)"><img src="img/exhibit/setup/tree/back1.png"></li>
                        <li onclick="thumbChange8(2)"><img src="img/exhibit/setup/tree/back2.png"></li>
                        <li onclick="thumbChange8(3)"><img src="img/exhibit/setup/tree/back3.png"></li>
                      </ul>
                </div>
           </div>
           

       
    </div>
    

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
@endsection



