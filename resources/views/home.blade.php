@extends('layouts.home')
@section('content')
<link href="/css/home.css" rel="stylesheet">
<body>
<div>
  <a>
  <div class="container">
    <img   href="{{ url('/') }}" src="img/banner.png" alt="Snow" style="width:100%;  height:150;">
    <div class="top-left"  ><a  style="color:white; font-size:3vw;" class="nav-link" href="kalusugan" >Kalusugan</a></div>
    <div class="top-right"><a style="color:white; font-size:3vw;"  class="nav-link" href="kabuhayan">Kabuhayan</a></div>
    <div class="bottom-right"><a  style="color:white; font-size:3vw;" class="nav-link" href="kinabukasan">Kinabukasan</a></div>
    <div class="bottom-left" ><a style="color:white; font-size:3vw;" class="nav-link" href="kaayusan">Kaayusan</a></div>
 
</a>
    <script>
    // Set the date we're counting down to
    var countDownDate = new Date("December 7, 2020 9:00:00").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

      // Get today's date and time
      var now = new Date().getTime();
        
      // Find the distance between now and the count down date
      var distance = countDownDate - now;
        
      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
      // Output the result in an element with id="demo"
      document.getElementById("demo").innerHTML = days + "d " + hours + "h "
      + minutes + "m " + seconds + "s ";
        
      // If the count down is over, write some text 
      if (distance < 0) {
        clearInterval(x);
        document.getElementById("demo").innerHTML = "EXPIRED";
      }
    }, 1000);
    </script>

</div>

</body>

@endsection
