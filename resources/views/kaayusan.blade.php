@extends('layouts.home')
@section('content')
<link href="/css/kaayusan.css" rel="stylesheet">

<div class="container text-center border">
    <div class="hello_world " id="images">
    <a href="setup#apo" > <img id="" class="img-fluid" alt="Setup"   src="img/icons/setup_icon.jpg" ></a>
        <a href="rstl#apo" ><img id="" class="img-fluid" alt="RSTL"  src="img/icons/rstl_icon.jpg" ></a>
        <a href="cest#apo" ><img id="" class="img-fluid" alt="cest"  src="img/icons/cest_icon.jpg" ></a>
        <a href="innovation#apo" ><img id="" class="img-fluid" alt="Innovation"  src="img/icons/innovation_icon.jpg" ></a>
        <a href="schoolar#apo" ><img id="" class="img-fluid" alt="Scholarship"   src="img/icons/scholars_icon.jpg" ></a>
    </div>
</div>

  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
  
    <ol class="carousel-indicators" id="">
    
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src=""  style="width:100%;">

        <script>
            function thumbChange(num){
                var thumb = 'img/exhibit/setup/Apo/back' + num + '.png';
                document.getElementById("mainImg").src = thumb;
            }

        </script>
       
       <div  id="apo" class="container" >

<div class="main" style="background-color:black; color: white;">
  <br><br>
<iframe width="540" height="315" src="https://www.youtube.com/embed/1HnKeyPmJGk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
        <div class="u-container-layout u-container-layout-2">
    
            <h2 class="text1" style="font-size: 2vw;">Metrology<br>
          </h2>
          <p class="text">
            <font style="font-size: .9vw;">The National Metrology Laboratory (NML) is the national metrology institute of the Philippines tasked by law to be responsible for establishing and maintaining the national measurement standards for physical quantities such as mass, temperature, pressure, voltage, resistance, luminous intensity and time interval and their dissemination to Filipino users. It has 27 staff and is located at the Metrology Building within the Department of Science and Technology Compound.  
            The main entrance to the compound is located at General Santos Avenue, Bicutan, Taguig City.The NML operates under the Industrial Technology Development Institute (ITDI), one of the agencies of the Department of Science and Technology (DOST).
             </font>
            <span id="dots">...</span><span id="more">
            <font  style="font-size: .9vw;">  DOST is the executive department of the Philippine Government responsible for the coordination of science and technology-related projects in the Philippines and for the formulation of policies and projects in the fields of science and technology in support of national development.
        </font>  </span>
              <br><br>
              <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore">Read More</h5>
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
              <script type="text/javascript">
                $(document).ready(function(){
                  $('#readMore').click(function(){
                    if($(this).html() == 'Read More'){
                      $('#dots').hide();
                      $('#more').show();
                      $(this).html('Read Less');

                    }else {
                      $('#dots').show();
                      $('#more').hide();
                      $(this).html('Read More');
                    }
                    });
                });
              </script>
          </p>
        </div>
      </div>
</div>


</div>

                


</div>


      <div class="item">
        <img src=""  style="width:100%;">
        <script>
            function thumbChange2(num){
                var thumb = 'img/exhibit/setup/fab/back' + num + '.png';
                document.getElementById("mainImg3").src = thumb;
            }

        </script>
        <div class="container">

        <div class="main" style="background-color:black; color: white;">
        <br><br>
        <iframe width="540" height="315" src="https://www.youtube.com/embed/2qhHPkXAeAk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
                    <div class="u-container-layout u-container-layout-2">
                    
                    <h2 class="text1" style="font-size: 2vw;">Vertical Helophyte Filter System<br>
                      </h2>
                      <p class="text">
                        <font style="font-size: .9vw;">Vertical Helophyte Filter System (VHFS) is a low-cost and nature-based secondary
                        wastewater treatment system. It is a well-known decentralized wastewater treatment
                        technology in the Netherlands where over 50,000 systems have already been installed.

                        By alternating the filter substrate, DOST XI improved the system in which the filter
                        becomes twice as effective as the conventional constructed wetland. This was proven
                        in multiple projects such as Porky Best (meat processing and septic waste), where
                        laboratory analyses have shown extraordinary results regarding BOD, COD, Ph, Coliform,
                        and Fecal Coliform removal.
                      </font>
                        <span id="dots">...</span><span id="more4">
                        <font  style="font-size: .9vw;">
                        To sufficiently address the current wastewater issues within Region XI, DOST XI will
                      share this technology by constructing one VHFS in every province under the Wastewater
                      Treatment System Upgrading Program (WATS-Up), a DOST-PCIEERD funded program to
                      address wastewater issues at public facilities; local micro, small, and medium enterprises
                      (MSMEs); and small communities.
                      </font>  </span>

                          <br><br>
                          <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore3">Read More</h5>
                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                          <script type="text/javascript">
                            $(document).ready(function(){
                              $('#readMore3').click(function(){
                                if($(this).html() == 'Read More'){
                                  $('#dots').hide();
                                  $('#more3').show();
                                  $(this).html('Read Less');
  
                                }else {
                                  $('#dots').show();
                                  $('#more3').hide();
                                  $(this).html('Read More');
                                }
                                });
                            });
                          </script>
                      </p>
                    </div>
                  </div>
           </div>
        

        </div>
      </div>
      <div class="item">
        <img src=""  style="width:100%;">
        <script>
            function thumbChange3(num){
                var thumb = 'img/exhibit/setup/chain/back' + num + '.png';
                document.getElementById("mainImg4").src = thumb;
            }

        </script>
        <div class="container">

        <div class="main" style="background-color:black; color: white;">
        <br><br>
        <iframe width="540" height="315" src="https://www.youtube.com/embed/v2VFPFFeb6I" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
                    <div class="u-container-layout u-container-layout-2">
                        <br>
                        <h2 class="text1" style="font-size: 2vw;">Microbiology
                      <br>
                      </h2>
                      <p class="text">
                        <font style="font-size: .9vw;">Microbiology Section
                        Conducts microbiological testing of food, water, cosmetics, disinfectants/ biological products or devices, natural products, herbal products, packaging materials and allied products
                        Pharmacology and Toxicology Section
                        Conducts pharmacological, toxicological testing of plant extracts, biological and chemical formulations, pesticides, insecticides, food supplements, drugs, pharmaceuticals products, herbal drug and preparations and cosmetics
                        Entomology Section
                        Conducts bioefficacy testing of household insecticides and screening of plant materials for insecticidal activity
                         </font>
                        <span id="dots">...</span><span id="more3">
                        <font  style="font-size: .9vw;"> 
                        </font>  </span>

                          <br><br>
                          <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore4">Read More</h5>
                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                          <script type="text/javascript">
                            $(document).ready(function(){
                              $('#readMore4').click(function(){
                                if($(this).html() == 'Read More'){
                                  $('#dots').hide();
                                  $('#more4').show();
                                  $(this).html('Read Less');
  
                                }else {
                                  $('#dots').show();
                                  $('#more4').hide();
                                  $(this).html('Read More');
                                }
                                });
                            });
                          </script>
                      </p>
                    </div>
                  </div>
           </div>
       

        </div>
      </div>
       
    </div>
    

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
@endsection



