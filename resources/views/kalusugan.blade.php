@extends('layouts.home')
@section('content')
<link href="/css/kalusugan.css" rel="stylesheet">

<div class="container text-center border">
    <div class="hello_world " id="images">
    <a href="setup#apo" > <img id="" class="img-fluid" alt="Setup"   src="img/icons/setup_icon.jpg" ></a>
        <a href="rstl#apo" ><img id="" class="img-fluid" alt="RSTL"  src="img/icons/rstl_icon.jpg" ></a>
        <a href="cest#apo" ><img id="" class="img-fluid" alt="cest"  src="img/icons/cest_icon.jpg" ></a>
        <a href="innovation#apo" ><img id="" class="img-fluid" alt="Innovation"  src="img/icons/innovation_icon.jpg" ></a>
        <a href="schoolar#apo" ><img id="" class="img-fluid" alt="Scholarship"   src="img/icons/scholars_icon.jpg" ></a>
    </div>
      
    </div>
</div>

  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
  
    <ol class="carousel-indicators" id="">
    
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src=""  style="width:100%;">

        <script>
            function thumbChange(num){
                var thumb = 'img/exhibit/setup/Apo/back' + num + '.png';
                document.getElementById("mainImg").src = thumb;
            }

        </script>
       
 <div  id="apo" class="container" >

            <div class="main" style="background-color:black; color: white;">
                <br><br>
            <iframe width="540" height="315" src="https://www.youtube.com/embed/ibUZOnyHRGw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
                    <div class="u-container-layout u-container-layout-2">
                        <br>
                        <h2 class="text1" style="font-size: 2vw;">Chips/Crunchies & Sesame Baby Food Blend<br>
                      </h2>
                      <p class="text">
                        <font style="font-size: .9vw;">The Davao del Norte State College (DNSC) in cooperation with the Food and Nutrition Research Institute (FNRI) and the Department of Science and Technology-Region XI is producing complementary food under the project name: Production of Cereal-Legume Complementary Foods for the Reduction of Malnourished Children in Region XI, a component of the national project entitled: “Roll-out of Complementary Food Production in the Regions”. The project is under the priority program of the Food and Nutrition Research Institute (FNRI) of the Department of Science and Technology (DOST) dubbed as the “Malnutrition Reduction Program (MRP)”. 
                        DNSC, a newly enrolled WME, is a foundation engaged in producing rice-mongo crunchies and rice-mongo sesame baby food blend. 
                         </font>
                        <span id="dots">...</span><span id="more">
                        <font  style="font-size: .9vw;">The target market for these complementary food products are infants after six (6) months of age and preschool children up to three (4) years old. These population groups have been identified as vulnerable to malnutrition particularly those belonging to the lower socio-economic level. DNSC’s Complementary Food Production Facility (CFPF) has been fully operational since September 2016. They are already approved by the FDA and have a License-To-Operate registration number of LTO-3000001736660. Recently, they had an in-house GMP Awareness Seminar participated by the production team and Food Technology students. Since then, there has been few applications of learnings to the enhancement of the production area and processing.

                    </font>  </span>
                          <br><br>
                          <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore">Read More</h5>
                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                          <script type="text/javascript">
                            $(document).ready(function(){
                              $('#readMore').click(function(){
                                if($(this).html() == 'Read More'){
                                  $('#dots').hide();
                                  $('#more').show();
                                  $(this).html('Read Less');
  
                                }else {
                                  $('#dots').show();
                                  $('#more').hide();
                                  $(this).html('Read More');
                                }
                                });
                            });
                          </script>
                      </p>
                    </div>
                  </div>
            </div>
          
            
        </div>

                            


      </div>

      <div class="item">
        <img src=""  style="width:100%;">
        <script>
            function thumbChange2(num){
                var thumb = 'img/exhibit/setup/fab/back' + num + '.png';
                document.getElementById("mainImg3").src = thumb;
            }

        </script>
        <div class="container">

        <div class="main" style="background-color:black; color: white;">
        <br><br>
        <iframe width="540" height="315" src="https://www.youtube.com/embed/stY_t7SwH44" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
                    <div class="u-container-layout u-container-layout-2">
                    
                      <h2 class="text1" style="font-size: 2vw;">Food Processing Innovation Center (FPIC)
                      <br>
                      </h2>
                      <p class="text">
                        <font style="font-size: .9vw;">The Food Processing Innovation Center (FPIC) – Davao is the first Regional Food Innovation Center (FIC) established through the High Impact Technology Solutions (HITS) program of the Department of Science and Technology (DOST). It serves as a hub of innovations and support services for value-adding of fresh produce and development of processed foods in Davao Region.
                        The main purpose of FPIC-Davao is to provide the local industry in Davao Region efficient facilities that would boost and develop their capabilities and productivities; to function as one-stop Research and Development (R&D) facilities in Davao Region where food processors, researchers, faculty, and students can innovate and create value-added products from local agricultural and fishery resources;
                        </font>
                        <span id="dots">...</span><span id="more3">
                        <font  style="font-size: .9vw;">  and to link with relevant local and international agencies in the planning, implementation, and evaluation of programs on food processing and product development. To realize its goal, the FPIC-Davao, Inc. will spearhead the development of value added agricultural and fishery food products in Davao Region through innovation. One of the main target outcomes of the project is to produce innovative food products. For the past 5 years, FPIC-Davao has developed 165 prototypes (101%) from the concepts generated using the DOST-developed food processing equipment namely spray dryer, water retort, vacuum fryer, and freeze dryer. 39 of these prototypes were already published in the Intellectual Property Office of the Philippines in which 3 were already registered as utility models. Recently, these 3 registered utility models were accepted as recipients of the 2020 DOST Intellectual Awards administered by the DOST-National Academy of Science and Technology (NAST). FPIC-Davao has also rendered 531 services (100%) including product development, trainings, and consultancy. It has also served 555 clients (105%) including students, faculty, researchers and start up food processing enterprises in Davao region.
                        The establishment of FPIC-Davao was recognized as 1 of the 10 best practices in the country for 2020 under the Government Best Practice Recognition administered by the Development Academy of the Philippines (DAP). The best practices of FPIC such as being the pioneer Food Innovation Center (FIC) in the Philippines, excellent example of synergistic multi-sectoral, multi-agency Collaboration for a project and strong compliance to applicable regulatory and statutory requirements were highlighted.
                        To align with standards and regulation, FPIC-Davao was registered under the Securities and Exchange Commission (SEC) bearing the name Food Processing Innovation Center – Davao, Inc. as a non-stock and non-profit organization in 2017. It also acquired its BIR registration, Business Permit and Fire safety inspection certificate in the following years. Recently, FPIC-Davao has applied for GMP certification to ensure integrity of its food manufacturing process as well as compliance with food safety regulations. This is in addition in the granted FDA-License To Operate as Food Manufacturer in 2017 to FPIC which is valid until 2024. 
                        In 2016, FPIC-Davao was the winner of the 2016 Davao Region Excellence Award in Project Implementation (DREAPI) (Economic Sector Category) by the Regional Development Council (RDC) XI. FPIC-developed product, Spray dried Buko powder was one of the 3 Most Innovative Products in the National Competition organized by DOST-ITDI.
                        Lastly, the center is also actively involved in the fight against Covid19 pandemic by producing immune-boosting beverages and healthy snacks for the frontliners. These products were previously developed in the center. Over 900 bottles of Ready-to-Drink beverages such as calamansi-blue ternate, calamansi-sweet potato and calamansi-ginger, and 300 packs of vacuum fried squash chips were distributed to several isolation facilities in the city including the Southeastern Philippines Medical Center.

                        As part of the sustainability plan of the center, FPIC will intensify the commercialization of the developed products and continue to serve as a national and international benchmark.

                        </font>  </span>
                          <br><br>
                          <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore3">Read More</h5>
                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                          <script type="text/javascript">
                            $(document).ready(function(){
                              $('#readMore3').click(function(){
                                if($(this).html() == 'Read More'){
                                  $('#dots').hide();
                                  $('#more3').show();
                                  $(this).html('Read Less');
  
                                }else {
                                  $('#dots').show();
                                  $('#more3').hide();
                                  $(this).html('Read More');
                                }
                                });
                            });
                          </script>
                      </p>
                    </div>
                  </div>
           </div>
        

        </div>
      </div>
      <div class="item">
        <img src=""  style="width:100%;">
        <script>
            function thumbChange3(num){
                var thumb = 'img/exhibit/setup/chain/back' + num + '.png';
                document.getElementById("mainImg4").src = thumb;
            }

        </script>
        <div class="container">

        <div class="main" style="background-color:black; color: white;">
        <br><br>
        <iframe width="540" height="315" src="https://www.youtube.com/embed/oPG3kdBxu5A" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
                    <div class="u-container-layout u-container-layout-2">
                        <br>
                      <h2 class="text1" style="font-size: 2vw;">DOST Pandemic Manual<br>
                      </h2>
                      <p class="text">
                        <font style="font-size: .9vw;">The DOST XI Pandemic Manual is a customized adaptation of the Pandemic Manual of the United States to address the specific needs and requirements of a government agency in the country in combatting a pandemic. It serves as a guide for employees in preparing, mitigating, and building resilience towards pandemic not only as a reactive strategy but as a proactive approach to dealing with pandemics and other health emergencies in the future. The PAN Manual, the first of its kind in the Philippines, is customized to the operations and functions of a government agency to address specific concerns and requirements of a pandemic that are not adequately addressed by the Disaster Risk Reduction and Management of the country and the established procedures of the Department of Health.
                      </font>
                        <span id="dots">...</span><span id="more4">
                        <font  style="font-size: .9vw;">
                        The PAN Manual was deployed to other government agencies, HEIs, and MSMEs for wider reach and applicability. It serves as a benchmark of other government agencies and private sector organizations for continued service delivery during health outbreaks. This engagement also hopes to influence policies for disaster risk reduction and effective response in time of a health outbreak.
                         
                      </font>  </span>
                          <br><br>
                          <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore4">Read More</h5>
                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                          <script type="text/javascript">
                            $(document).ready(function(){
                              $('#readMore4').click(function(){
                                if($(this).html() == 'Read More'){
                                  $('#dots').hide();
                                  $('#more4').show();
                                  $(this).html('Read Less');
  
                                }else {
                                  $('#dots').show();
                                  $('#more4').hide();
                                  $(this).html('Read More');
                                }
                                });
                            });
                          </script>
                      </p>
                    </div>
                  </div>
           </div>
       

        </div>
      </div>
      <div class="item">
        <img src=""  style="width:100%;">
        <script>
            function thumbChange4(num){
                var thumb = 'img/exhibit/Innovation/Starbooks/back' + num + '.png';
                document.getElementById("mainImg5").src = thumb;
            }

        </script>
        <div class="container">

        <div class="main" style="background-color:black; color: white;">
            <img id="mainImg5" src="img/exhibit/Innovation/Starbooks/back1.png" >
            <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
                    <div class="u-container-layout u-container-layout-2">
                        <br>
                        <h2 class="text1" style="font-size: 2vw;">Science and Technology Academic and Research-Based Openly Operated Kiosks (STARBOOKS)<br>
                      </h2>
                      <p class="text">
                        <font style="font-size: .9vw;">The first STARBOOKS and Solar Powered STARBOOKS in the Philippines were installed at Davao Region. STARBOOKS aims to provide learners and learning institutions with a tool to gain knowledge even without internet. In total, DOST XI have deployed 327 STARBOOKS. DOST XI innovated, produced and deployed Solar Powered STARBOOKS to provide learners at Geographically Isolated and Disadvantaged Areas with means to gain knowledge even without internet and even without electricity. Currently, DOST XI have deployed 5 Solar Powered STARBOOKS in the Region.      </font>
                        <span id="dots">...</span><span id="more10">
                        <font  style="font-size: .9vw;">
                        DOST XI puts prime in partnering with different instutitions. Currently, DOST XI has forged partnerships with: DepEd XI, TESDA XI, DTI XI, Therma South Inc. (Aboitiz) and University of Mindanao, LGU Davao City, LGU Digos City and United Nations World Food Programme.
                        </font>  </span>

                          <br><br>
                          <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore5">Read More</h5>
                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                          <script type="text/javascript">
                            $(document).ready(function(){
                              $('#readMore5').click(function(){
                                if($(this).html() == 'Read More'){
                                  $('#dots').hide();
                                  $('#more5').show();
                                  $(this).html('Read Less');
  
                                }else {
                                  $('#dots').show();
                                  $('#more5').hide();
                                  $(this).html('Read More');
                                }
                                });
                            });
                          </script>
                      </p>
                    </div>
                  </div>
           </div>
        <ul class="thumbs">
        <li onclick="thumbChange4(1)"><img src="img/exhibit/Innovation/Starbooks/back1.png"></li>
            <li onclick="thumbChange4(2)"><img src="img/exhibit/Innovation/Starbooks/back2.png"></li>
            <li onclick="thumbChange4(3)"><img src="img/exhibit/Innovation/Starbooks/back3.png"></li>
            
            
        
    

        </ul>

        </div>
      </div>
       
    </div>
    

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
@endsection



