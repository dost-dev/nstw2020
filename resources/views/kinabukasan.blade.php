@extends('layouts.home')
@section('content')
<link href="/css/kinabukasan.css" rel="stylesheet">

<div class="container text-center border">
    <div class="hello_world " id="images">
    <a href="setup#apo" > <img id="" class="img-fluid" alt="Setup"   src="img/icons/setup_icon.jpg" ></a>
        <a href="rstl#apo" ><img id="" class="img-fluid" alt="RSTL"  src="img/icons/rstl_icon.jpg" ></a>
        <a href="cest#apo" ><img id="" class="img-fluid" alt="cest"  src="img/icons/cest_icon.jpg" ></a>
        <a href="innovation#apo" ><img id="" class="img-fluid" alt="Innovation"  src="img/icons/innovation_icon.jpg" ></a>
        <a href="schoolar#apo" ><img id="" class="img-fluid" alt="Scholarship"   src="img/icons/scholars_icon.jpg" ></a>
    
      
    </div>
</div>

  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
  
    <ol class="carousel-indicators" id="">
    
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
     
    
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src=""  style="width:100%;">

        <script>
            function thumbChange(num){
                var thumb = 'img/exhibit/setup/Apo/back' + num + '.mp4';
                document.getElementById("mainImg").src = thumb;
            }

        </script>
       
<div  id="apo" class="container" >

<div class="main" style="background-color:black; color: white;">
    
   <br><br>
<iframe width="540" height="315" src="https://www.youtube.com/embed/2u90MfUkrCs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


    <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
        <div class="u-container-layout u-container-layout-2">
            
        <h2 class="text1" style="font-size: 2vw;">DIGIHUB Project


            </h2>
            <p class="text">
              <font style="font-size: .9vw;">The Digital Design and Prototyping Hub (DigiHub) project is a collaboration between DOST XI, Department of Trade and Industry (DTI) XI and the University of Southeastern Philippines (USeP) which aims to provide a facility where students, MSMEs, hobbyists and inventors can develop product designs and prototypes.
                Digihub – Fabrication Laboratory (FabLab) Davao was launched on February 15, 2019 and is now operating in a limited capacity.Last August 2019, Fablabs.io recognized Digihub – FABLAB Davao as part of the International FabLab Organization. Through this, Digihub became a cooperator of Norde International in holding an event such as Accelerating Makers to promote makerspace movement in the country. 
                In addition, Digihub – FABLAB Davao collaborated and participated a digital fabrication workshop on Furniture, Bamboo Design, Wearables, Textiles and Packaging for Food and Non-Food facilitated by Norde International.  
              </font>
              <span id="dots">...</span><span id="more2">
              <font  style="font-size: .9vw;">    Prospect users of FabLab including MSMEs, industry associations and schools attended the event.
              The facility catered several trainings already attended with clients from different sectors such as students, faculty, and MSMEs. For the year 2019, there are 14 trainings conducted and 145 people trained. The facility had also accommodated 167 visitors inquiring for the services offered in the FabLab. Digihub staffs had also undergone 2 trainings to be equipped in operating the equipment. 
              Equipment in DigiHub include 3D printers for creating solid items,  laser cutter,  wide format printer, 3D scanner for capturing solid objects into 3D models, a UV printer for non-paper printing, vacuum forming machine, heat press, digital embroidery machine, a Computer Numerical Control (CNC) machine and graphics workstations for computer graphics design and 3d modeling.
              During the first wave of the Covid19 pandemic, there was shortage of PPE materials for frontliners, especially face shields and intubation boxes. DigiHub immediately produced the much needed PPE and was able to fabricate 6,278 face shields distributed to 47 hospitals, rural health units, police checkpoints and LGUs throughout the region. 36 aerosol intubation boxes were also provided to 3 covid hospitals. 

              </font>  </span>
              <br><br>
              <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore">Read More</h5>
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
              <script type="text/javascript">
                $(document).ready(function(){
                  $('#readMore').click(function(){
                    if($(this).html() == 'Read More'){
                      $('#dots').hide();
                      $('#more').show();
                      $(this).html('Read Less');

                    }else {
                      $('#dots').show();
                      $('#more').hide();
                      $(this).html('Read More');
                    }
                    });
                });
              </script>
          </p>
        </div>
      </div>
</div>


</div>

                


</div>

<div class="item">
<img src=""  style="width:100%;">
<script>
function thumbChange2(num){
    var thumb = 'img/exhibit/setup/fab/back' + num + '.png';
    document.getElementById("mainImg2").src = thumb;
}

</script>
<div class="container">

<div class="main" style="background-color:black; color: white;">
<br><br>
<iframe width="540" height="315" src="https://www.youtube.com/embed/watch?v=bLgQdko7BXM&list=PLl6a_Y8anztl5RJVN9OE9RhGJ7pebi6Eo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
        <div class="u-container-layout u-container-layout-2">
        
        <h2 class="text1" style="font-size: 2vw;">DOST SEI<br>
          </h2>
          <p class="text">
          <font style="font-size: .9vw;">In the midst of the Coronavirus pandemic, the Department of Science and Technology (DOST) calls on the youth to join the science community by pursuing careers in science, technology, engineering, and mathematics (STEM).
            This, as DOST’s Science Education Institute (SEI), announces the availability of scholarship slots for the 2021 DOST-SEI S&T Undergraduate Scholarships for incoming Grade 12 students in AY 2020-2021 who intend to pursue STEM courses when they enroll in college next year. The scholarship program aims to push for knowledge-driven development through S&T human resource advancement. 
            “Amid the uncertainty, we soldier on in providing scholarship opportunities for college studies.   The DOST Scholarship invites talented Filipino youth to pursue STEM-related careers and take active participation in research and development (R&D) activities. We hope to have more scientists especially in this changing world” DOST-SEI Director, Dr. Josette T. Biyo, said. Applicants for the scholarship must be natural-born Filipinos of good moral character and in good health.
            </font>
            <span id="dots">...</span><span id="more3">
            <font  style="font-size: .9vw;">
            Application is open to students in the STEM strand.
            Those in the non-STEM strand may also apply provided they belong to the top 5% of their graduating class.
          Students who belong to families whose socio-economic status does not exceed the cut-off values for certain indicators may apply for RA 7687 program, otherwise, applicants may apply for the Merit Scholarship Program.
          To be able to enjoy the scholarship, applicants should pass the 2021 S&T Undergraduate Scholarships Examination and pursue a Bachelor of Science degree program in any of the priority fields of study at a state university or college or any private higher education institution that is recognized by the Commission on Higher Education as Center of Excellence or Center of Development or with at least FAAP Level III accreditation for the BS program that they intend to enroll in. For the complete list of requirements, priority programs, and study placements, visit SEI's website at www.sei.dost.gov.ph.
          Interested applicants may apply online through https://www.science-scholarships.ph/. The deadline for online application is on 28 August 2020. The date of nationwide qualifying examination is on 25 October 2020. (DOST-SEI Press Release)
        </font>  </span>
              <br><br>
              <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore2">Read More</h5>
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
              <script type="text/javascript">
                $(document).ready(function(){
                  $('#readMore2').click(function(){
                    if($(this).html() == 'Read More'){
                      $('#dots').hide();
                      $('#more2').show();
                      $(this).html('Read Less');

                    }else {
                      $('#dots').show();
                      $('#more2').hide();
                      $(this).html('Read More');
                    }
                    });
                });
              </script>
          </p>
        </div>
      </div>
</div>


</div>
</div>



       
    </div>
    

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
@endsection



