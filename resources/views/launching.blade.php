@extends('layouts.home')
@section('content')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<link href="/css/launching.css" rel="stylesheet">
<div class="container">
    <h2 class="center1"><strong>VIRTUAL NSTW </strong></h2>
        <p class="center">PROGRAM OF ACTIVITIES</p><br>
    <br>
<table class="table table-striped">
  <thead >
    <tr class="th">
      <th class="li" scope="col">TIME</th>
      <th class="li" scope="col">TITLE OF ACTIVITY</th>
      <th class="li" scope="col">THEMATIC CLASSIFICATION</th>
      <th class="li" scope="col">ONLINE PLATFORM</th>
      <th class="li" scope="col">RESOURCE/PERSON/SEAKER</th>
      <th class="li" scope="col">SHORT DESCRIPTION OF ACTIVITY</th>
      
    </tr>
  </thead>
  <tbody>
    <tr style="height: 54px;">
                
                <td colspan="6" class="date" ><strong><a style="color:white;">December 7, 2020</a></strong></td>
                
    </tr>
      
      <td style="font-size:.9vw;color:white;">9:00 a.m.</td>
      <td style="font-size:.9vw;color:white;">Online Launch of Davao RSTW Virtual Exhibit</td>
      <td style="font-size:.9vw;color:white;">All</td>
      <td style="font-size:.9vw;color:white;">Zoom</td>
      <td style="font-size:.9vw;color:white;">TBA</td>

      <td style="font-size:.9vw;color:white;" >Online Launch of Davao RSTW Virtual Exhibit<br>
      <a href="https://zoom.us/j/99398928962?pwd=WFNtcWhSWGJyM1ZhcUhKYVF5a0E4UT09" style="text-decoration:none;" >
      LINK
      </a>
    </td>
    </tr>
    <tr>
     
      <td style="font-size:.9vw; color:white;">2:00 p.m.</td>
      <td style="font-size:.9vw;color:white;">Online Forum 1 / Computing and Archiving Research Environment (COARE)</td>
      <td style="font-size:.9vw;color:white;">Kaayusan</td>
      <td style="font-size:.9vw;color:white;">Zoom</td>
      <td style="font-size:.9vw;color:white;">MS. JELINA TANYA TETANGCO
        Senior Science Research Specialist
        DOST-ASTI</td>
      <td style="font-size:.9vw;color:white;"> COARE facility has provided compute and storage resources, as well as continuous technical support to DOST's COVID-19 initiativest<br>
      <a href="https://zoom.us/j/95468810047?pwd=a1B5aGh3cHl2dFpqVmNrYWpRUUFpQT09" style="text-decoration:none;" >
      LINK
      </a>
    </td>
     
    </tr>
    <tr style="height: 54px;">
                
                <td colspan="6" class="date"><strong><a style="color:white;">December 8, 2020</a></strong></td>
                
    </tr>
      
      <td style="font-size:.9vw;color:white;">9:00 a.m.</td>
      <td style="font-size:.9vw;color:white;">Virtual Screening of Science Short Films (Contest)</td>
      <td style="font-size:.9vw;color:white;">Kinabukasan</td>
      <td style="font-size:.9vw;color:white;">Zoom</td>
      <td style="font-size:.9vw;color:white;">Joel Leonard </td>
      <td style="font-size:.9vw;color:white;"> Science short film contest highlighting/ depicting the 2020 NSTW Theme <br>
      <a href="https://zoom.us/j/94606610160?pwd=OXVRdk9WcWdoK2NSNlg0TTFuUy9BZz09" style="text-decoration:none;" >
      LINK
      </a>
      </td>
    </tr>
      <td style="font-size:.9vw;color:white;">10:00 am</td>
      <td style="font-size:.9vw;color:white;"> Youth Excellence in Science (YES) Award</td>
      <td style="font-size:.9vw;color:white;">Kinabukasan</td>
      <td style="font-size:.9vw;color:white;">Zoom</td>
      <td style="font-size:.9vw;color:white;">MR. JOEL LEONARD
        Supporter, Nations Maker
        Workforce Development Consultant
        North Carolina, USA</td>
      <td style="font-size:.9vw;color:white;">Youth Excellence in Science (YES) Award <br>
      <a href=" https://zoom.us/j/99519301347?pwd=dlNjN3NOTVBhWHVHRnlIcVBkM1A3UT09" style="text-decoration:none;" >
      LINK
      </a>
      </a>
    </td>
    </tr>
   
      
      <td style="font-size:.9vw;color:white;">2:00 p.m.</td>
      <td style="font-size:.9vw;color:white;">Online Forum 2/The Pandemic Makerspace</td>
      <td style="font-size:.9vw;color:white;">Kinabukasan</td>
      <td style="font-size:.9vw;color:white;">Youtube</td>
      <td style="font-size:.9vw;color:white;">TBA</td>
      <td style="font-size:.9vw;color:white;"> Talk related to the experiences of fablabs/makers during the Covid19 pandemic
    <br>
    <a href=" https://zoom.us/j/93402452392?pwd=RDQwVUNiMDh4Z0tPSlhpL2d2QVp2QT09" style="text-decoration:none; " >
    LINK
    </a>
    </td> 
    </tr>
    <tr style="height: 54px;">
                
                <td colspan="6" class="date"><strong><a style="color:white;">December 9, 2020</a></strong></td>
                
    </tr>
      
      <td style="font-size:.9vw;color:white;">9:00 a.m.</td>
      <td style="font-size:.9vw;color:white;">Virtual Opening of Science Photography Gallery (Contest)</td>
      <td style="font-size:.9vw;color:white;">All</td>
      <td style="font-size:.9vw;color:white;">Instagram</td>
      <td style="font-size:.9vw;color:white;">TBA</td>
      <td style="font-size:.9vw;color:white;"> Online Photo Contest on NSTW Theme
      <br>
      <a href="  https://zoom.us/j/99390220012?pwd=M1RTRmFqclI0eVp4UHFFdGhQTHNLUT09" style="text-decoration:none;" >
      LINK
      </a>
    </td>
    </tr>
    <tr>      
      <td style="font-size:.9vw;color:white;">2:00  p.m.</td>
      <td style="font-size:.9vw;color:white;">Advance Manufacturing Center(AMCEN</td>
      <td style="font-size:.9vw;color:white;">Kinabukasan</td>
      <td style="font-size:.9vw;color:white;">Zoom</td>
      <td style="font-size:.9vw;color:white;">TBA/MIRDC</td>
      <td style="font-size:.9vw;color:white;"> Talk about AMCEN's facilities and services that can be accessed by indutries in Davao Region
      <br>
      <a href="  https://zoom.us/j/97857124987?pwd=OHNrZjIxYlg5Z0FncmZIUzEvSnVsQT09" style="text-decoration:none;" >
      LINK
      </a>
    </td> 
    </tr>

    <tr style="height: 54px;">
                
                <td colspan="6" class="date"><strong><a style="color:white;">December 10, 2020</a></strong></td>
                
    </tr>
      
      <td style="font-size:.9vw;color:white;">9:00 a.m.</td>
      <td style="font-size:.9vw;color:white;">Video Launch of DOST Funded  Projects</td>
      <td style="font-size:.9vw;color:white;">All</td>
      <td style="font-size:.9vw;color:white;">Zoom</td>
      <td style="font-size:.9vw;color:white;">N/A</td>
      <td style="font-size:.9vw;color:white;"> Launching of Sentro Mindanaw / AGAK Center / DigiHub / VHFS / PGC / Covid Testing Centers / Pan Manual 
      <br>
      <a href="https://zoom.us/j/99972281856?pwd=dldYMXpmYUlTN2xCOTFpTVlSUGRydz09" style="text-decoration:none;" >
      LINK
      </a>
    
    </td>
    </tr>
    <tr>      
      <td style="font-size:.9vw;color:white;">2:00 p.m.</td>
      <td style="font-size:.9vw;color:white;">Philippince Earth Data Resource and Observation (PEDRO) Center</td>
      <td style="font-size:.9vw;color:white;">Kaayusan</td>
      <td style="font-size:.9vw;color:white;">Zoom</td>
      <td style="font-size:.9vw;color:white;">MR. RANDY C. BEROS
      Science Research Specialist II
      DOST-ASTI</td></td>
      <td style="font-size:.9vw;color:white;"> Talk on ASTI's Ground Receiving Station (GRS) facility with direct access to a broad range of optical (high-resolution, multispectral) and radar (cloud penetrating, day-night-imaging) satellite data.
      <br>
      <a href="https://zoom.us/j/98415942393?pwd=Q3I0SkdJdEVORWJMMkVlbWFmKzVMUT09" style="text-decoration:none;" >
      LINK
      </a>
    </td>
    </tr>
    <tr style="height: 54px;">
                
                <td colspan="6" class="date"><strong><a style="color:white;">November 11, 2020</a></strong></td>
                
    </tr>
      
      <td style="font-size:.9vw;color:white;">9:00 a.m.</td>
      <td style="font-size:.9vw;color:white;">Online Forum 5/ Project SARAI : Cacao Primary Processing Technologies</a>
      <td style="font-size:.9vw;color:white;">Kabuhayan</td>
      <td style="font-size:.9vw;color:white;">Zoom</td>
      <td style="font-size:.9vw;color:white;">DR. CALIXTO M. PROTACIO
      Project Leader, SARAI Project 1.3: Phenology
      Studies, Crop Management & Model
      Development for Coffee and Cacao</td>
      <td style="font-size:.9vw;color:white;"> Webinar on SARAI Technologies for smarter agriculture 
      <br>
      <a href="https://zoom.us/j/93083132730?pwd=UUg5c2V0TnJOc2FXZ2ZIMzVhWG5xdz09" style="text-decoration:none;" >
      LINK
      </a>
    </td>
    </tr>
    <tr>      
      <td style="font-size:.9vw;color:white;">2:00 p.m.</td>
      <td style="font-size:.9vw;color:white;">Philippince Earth Data Resource and Observation (PEDRO) Center</td>
      <td style="font-size:.9vw;color:white;">Kabuhayan</td>
      <td style="font-size:.9vw;color:white;">Zoom</td>
      <td style="font-size:.9vw;color:white;">PROF. NELIO ALTOVEROS
      Project Leader, SARAI Project 2.2: Enhanced
      Operation and Connectivity of Automatic
      Weather Station and Unmanned Aerial Vehicle
      Units for Crop-Environment Monitoring and </td>
      <td style="font-size:.9vw;color:white;"> Online Forum 6/ Project SARAI : Weather Information and Drone Aerial Images for Crop Monitoring 
      <br>
      <a href="https://zoom.us/j/95700171825?pwd=ckIweDBNNFFCVWlyVFhjL2ZMeFJ4UT09" style="text-decoration:none;" >
      LINK
      </a>
    
    
    </tr> 
  </tbody>
</table>

</div>

@endsection