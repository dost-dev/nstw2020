<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <link rel="shortcut icon" href="{{ url('images/dost-logo.png') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>NSTW 2020</title>
    @yield('before-styles')
    <link rel="stylesheet" href="{{asset('css/nicepage.css')}}" media="screen">
    <link rel="stylesheet" href="{{asset('css/Login.css')}}" media="screen">
    @yield('after-styles')
    <script class="u-script" type="text/javascript" src="{{asset('js/jquery.js')}}" defer=""></script>
    <script class="u-script" type="text/javascript" src="{{asset('js/nicepage.js')}}" defer=""></script>
    <meta name="generator" content="Nicepage 2.29.5, nicepage.com">
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Page Header</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.mn.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>


<style>
	.site-footer
	{
	  background-color:#26272b;
	  padding:45px 0 20px;
	  font-size:15px;
	  line-height:24px;
	  color:white;
	}
	.site-footer hr
	{
	  border-top-color:#bbb;
	  opacity:0.5
	}
	.site-footer hr.small
	{
	  margin:20px 0
	}
	.site-footer h6
	{
	  color:white;
	  font-size:16px;
	  text-transform:uppercase;
	  margin-top:5px;
	  letter-spacing:2px
	}
	.site-footer a
	{
	  color:white;
	}

	
  
    </style>

<footer class="site-footer">
		<div class="container">
		  <div class="row">
			<div class="col-sm-12 col-md-6">
            <p class="u-text u-text-1" style="text-align:left;">
           
            <img src="img/nstw.png" class="img-rounded" alt="Cinque Terre" width="350" height="150">
   
              </p>


			 			</div>
	
			<div class="col-sm-12 col-md-3" style="text-align:left;">
			<br>
                <font style="font-size: 18px;">DEPARTMENT OF SCIENCE AND TECHNOLOGY</font></b>
                <br><b>
                  <font style="font-size: 15px;">REGIONAL OFFICE NO. XI</font></b>
                <br>Cor. Friendship and Dumanlas Roads, Bajada,&nbsp;<br>Davao City, Philippines&nbsp;<br><br>
			</div>
	
			<div class="col-xs-6 col-md-3">
			 
              <ul class="footer-links"style="text-align:left;margin-left:28%;">
                  
                <a href=" https://facebook.com/dostdavaoregion" style="font-size:14px;"><img src="img/2b.png" class="img-rounded" alt="Cinque Terre" width="15" height="15"> @dostregion11  </a>
                <br><br><a href=" https://facebook.com/dostdavaoregion"></a><a href=" https://facebook.com/dostdavaoregion" class="fa fa-facebook" style="font-size:14px;"> &nbsp;&nbsp;@dostregion11  </a>
                <br><br><a href=" https://twitter.com/DOSTRegion11"></a><a href=" https://twitter.com/DOSTRegion11" class="fa fa-twitter" style="font-size:14px;">&nbsp;&nbsp;@DOSTRegion11 </a>
                <br><br><a href="https://www.instagram.com/dostregion11/"></a><a href="https://www.instagram.com/dostregion11/" class="fa fa-instagram" style="font-size:14px;">&nbsp;&nbsp; @dostregion11 </a>

			  </ul>
			</div>
		  </div>
		  <hr>
		</div>
		<div class="container">
		  <div class="row">
			<div class="col-md-8 col-sm-6 col-xs-12" style="text-align:left;">
                <font style="font-size: 12px;">© 2020 National Science and Technology Week (NSTW)&nbsp;Davao Region&nbsp;</font>
            
			</div>
	
			
		  </div>
		</div>
	  </footer>