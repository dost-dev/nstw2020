<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport" content="width=device-width, initial-scale=1">
 

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <meta charset="utf-8">
 

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        body {
            background-color: #E0E0E0 !important;
        }
        .site-footer
        {
          background-color:#26272b;
          padding:45px 0 20px;
          font-size:15px;
          line-height:24px;
          color:white;
        }
        .site-footer hr
        {
          border-top-color:#bbb;
          opacity:0.5
        }
        .site-footer hr.small
        {
          margin:20px 0
        }
        .site-footer h6
        {
          color:white;
          font-size:16px;
          text-transform:uppercase;
          margin-top:5px;
          letter-spacing:2px
        }
        .site-footer a
        {
          color:white;
        }
        .form-group {
            margin-bottom: 5px;
        }
        </style>

</head>
<body>
<div id="app" style="background-image:url(img/login1.png);"> 
    <nav class="navbar navbar-expand-md navbar-light bg-white sticky-top">
            <div class="container">
                 <a class="navbar-brand" href="{{ url('/home') }}">
                 <font> <img src="img/logohome.png" width="300" height="40" href="{{ url('/home') }}" alt=""></font>
                </a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link"  href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
						@else
						<li class="nav-item">
							<a class="nav-link"  href="launching"><font style="color:black; font-size:18px; font-style:opensans;"> Launching</font></a>
							</li>
							<li class="nav-item">
							<a class="nav-link"  href="exhibit"><font style="color:black; font-size:18px; font-style:opensans;"> Exhibit</font></a>
							</li>
							<li class="nav-item">
							<a class="nav-link"   href="webinar"><font style="color:black; font-size:18px; font-style:opensans;">  Webinar</font></a>
							</li>
                            <li class="nav-item dropdown">
                                
                                <a id="navbarDropdown" class="nav-link  dropdown-toggle"  style="font-size:15px;" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre><font style="color:black; font-size:18px; font-style:opensans;">  
                                {{ Auth::user()->name }} </front>
                                </a>


                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                       <font style="color:black; font-size:18px; font-style:opensans;">   {{ __('Logout') }}</font>
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    
</body>
<footer class="site-footer">
		<div class="container">
		  <div class="row">
			<div class="col-sm-12 col-md-6">
            <p class="u-text u-text-1" style="text-align:left; marging-right:">
           
            <img src="img/nstw.png"  alt="Responsive image" class="img-rounded" alt="Cinque Terre" width="350" height="150">
   
              </p>


			 			</div>
	
			<div class="col-sm-12 col-md-3" style="text-align:left; ">
			
                <font style="font-size: 1vw;">DEPARTMENT OF SCIENCE AND TECHNOLOGY</font></b>
                <br><b>
                  <font style="font-size: 1vw;">REGIONAL OFFICE NO. XI</font></b>
                <br > <font style="font-size: 1vw;">Cor. Friendship and Dumanlas Roads, Bajada,&nbsp;<br>Davao City, Philippines&nbsp;</font><br><br>
			</div>
	
			<div class="col-xs-6 col-md-3">
			 
              <ul class="footer-links"style="text-align:left;margin-left:28%;">
                  
                <a href=" https://facebook.com/dostdavaoregion" style="font-size:.9vw;"><img src="img/2b.png" class="img-rounded" alt="Cinque Terre" width="15" height="15"> @dostregion11  </a>
                <br><br><a href=" https://facebook.com/dostdavaoregion"></a><a href=" https://facebook.com/dostdavaoregion" class="fa fa-facebook" style="font-size:.9vw;"> &nbsp;&nbsp;@dostregion11  </a>
                <br><br><a href=" https://twitter.com/DOSTRegion11"></a><a href=" https://twitter.com/DOSTRegion11" class="fa fa-twitter" style="font-size:.9vw;">&nbsp;&nbsp;@DOSTRegion11 </a>
                <br><br><a href="https://www.instagram.com/dostregion11/"></a><a href="https://www.instagram.com/dostregion11/" class="fa fa-instagram" style="font-size:.9vw;">&nbsp;&nbsp; @dostregion11 </a>

			  </ul>
			</div>
		  </div>
		  <hr>
		</div>
		<div class="container">
		  <div class="row">
			<div class="col-md-8 col-sm-6 col-xs-12" style="text-align:left;">
                <font style="font-size: 1vw;">© 2020 National Science and Technology Week (NSTW)&nbsp;Davao Region&nbsp;</font>
            
			</div>
	
			
		  </div>
		</div>
	  </footer>

</html>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
var APP_URL = "{{ url('/') }}";
$(document).ready(function() {
    // alert(APP_URL);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#frm_register").on("submit", function() {
        var formData = new FormData($(this)[0]);
            // formData.append('_method','post')
            $.ajax({
                type: "POST",
                url: APP_URL + '/register',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if(data.err!=undefined)
                    {
                        let err="";
                        $.each(data.err, function(data_err, msg) {
                        // console.log(msg);
                        err += "<li>"+msg[0]+"</li>";
                        });

                        $("#reg_error").addClass("alert alert-danger");
                        $("#reg_error ul").html(err);
                    }else if(data.msg!=undefined)
                    {
                        $("#first_name").val("");
                        $("#middle_name").val("");
                        $("#last_name").val("");
                        $("#sex").val("");
                        $("#age").val("");
                        $("#institution").val("");
                        $("#designation").val("");
                        $("#contact").val("");
                        $("#sectoral_affiliation").val("");
                        $("#email").val("");
                        $("#password").val("");
                        $("#password-confirm").val("");
                        swal(data.msg);
                    }
                },
                error: function (data)
                {
                    console.log('Error:', data);
                }
            });
            return false;
    });

    $("#frm_login").on("submit", function() {
        var formData = new FormData($(this)[0]);
            // formData.append('_method','post')
            $.ajax({
                type: "POST",
                url: APP_URL + '/login',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if(data.e!=undefined)
                    {
                        // let err="";
                        // $.each(data.e, function(data_err, msg) {
                        // console.log(msg);
                        // err += "<li>"+msg[0]+"</li>";
                        // });

                        // $("#login_error").addClass("alert alert-danger");
                        $("#login_error ul").html("<li>"+data.e+"</li>");
                        $("#login_error ul li").css("color", "red");
                    }else if(data.s!=undefined)
                    {
                        window.location.replace(APP_URL+"/home");
                    }
                },
                error: function (data)
                {
                    console.log('Error:', data);
                }
            });
            return false;
    });
});
</script>
