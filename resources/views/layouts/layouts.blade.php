<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ url('img/dost-logo.png') }}">
    {{--  --}}
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <title>NSTW 2020</title>
    @yield('before-styles')
    {{-- <link rel="stylesheet" href="{{asset('css/nicepage.css')}}" media="screen"> --}}
    {{-- <link rel="stylesheet" href="{{asset('css/Login.css')}}" media="screen"> --}}
    @yield('after-styles')
    {{-- <script class="u-script" type="text/javascript" src="{{asset('js/jquery.js')}}" defer=""></script> --}}
    {{-- <script class="u-script" type="text/javascript" src="{{asset('js/nicepage.js')}}" defer=""></script> --}}
    <meta name="generator" content="Nicepage 2.29.5, nicepage.com">
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Page Header</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    {{-- <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script> --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/> --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.mn.js"></script> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    {{--  --}}

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        body {
            background-color: #E0E0E0 !important;
        }
        .site-footer
        {
          background-color:#26272b;
          padding:45px 0 20px;
          font-size:15px;
          line-height:24px;
          color:white;
        }
        .site-footer hr
        {
          border-top-color:#bbb;
          opacity:0.5
        }
        .site-footer hr.small
        {
          margin:20px 0
        }
        .site-footer h6
        {
          color:white;
          font-size:16px;
          text-transform:uppercase;
          margin-top:5px;
          letter-spacing:2px
        }
        .site-footer a
        {
          color:white;
        }
        .form-group {
            margin-bottom: 5px;
        }
        </style>
</head>
<body>
<div id="app" style="background-color:lightgray;"> 
        <nav class="navbar navbar-expand-md navbar-light bg-white sticky-top">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <font> <img src="img/logo2020.png"width="300" height="40"  alt=""></font>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="modal" data-target="#loginModal"><b>{{ __('Login') }}</b></a>
                            </li>
                            {{-- @if (Route::has('register')) --}}
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-toggle="modal" data-target="#registerModal"><b>{{ __('Register') }}</b></a>
                                </li>
                            {{-- @endif --}}
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        
        <main class="py-4">
            @yield('content')
        </main>
    </div>

<!-- The Login Modal -->
<div class="modal fade" id="loginModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" style="background-color:#ffa602;color:white;">
          <h4 class="modal-title">Login</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            {{-- <div class="card-body"> --}}
                <div id="login_error" class="offset-md-4"><ul></ul></div>
                <form method="POST" action="#" id="frm_login">
                    @csrf
                    <div class="form-group row">
                        <label for="uemail" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                        <div class="col-md-6">
                            <input id="uemail" type="email" class="form-control @error('uemail') is-invalid @enderror" name="uemail" value="{{ old('email') }}" required autocomplete="off" autofocus>

                            @error('uemail')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="upassword" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                        <div class="col-md-6">
                            <input id="upassword" type="password" class="form-control @error('upassword') is-invalid @enderror" name="upassword" required autocomplete="off">

                            @error('upassword')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary" style="background-color:black;">
                                {{ __('Login') }}
                            </button>

                           
                        </div>
                    </div>
                </form>
            {{-- </div> --}}
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  
</div>

<!-- The Registration Modal -->
<div class="modal fade" id="registerModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" style="background-color:#ffa602;color:white;">
          <h4 class="modal-title">Register</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <div id="reg_error"><ul></ul></div>

            <form id="frm_register">
                @csrf
                <div class="form-group row">
                    <label for="first_name" class="col-md-1 col-form-label text-md-right"></label>

                    <div class="col-md-10">
                        <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="fname" value="{{ old('first_name') }}" placeholder="{{ __('First name') }}" required autocomplete="first_name" autofocus>

                        @error('first_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    {{-- <label for="middle_name" class="col-md-4 col-form-label text-md-right">{{ __('Middle name') }}</label> --}}
                    <label for="middle_name" class="col-md-1 col-form-label text-md-right"></label>
                    <div class="col-md-10">
                        <input id="middle_name" type="text" class="form-control @error('middle_name') is-invalid @enderror" name="mname" placeholder="{{ __('Middle name') }}" value="{{ old('middle_name') }}" required autocomplete="middle_name" autofocus>

                        @error('middle_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="last_name" class="col-md-1 col-form-label text-md-right"></label>

                    <div class="col-md-10">
                        <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="lname" placeholder="{{ __('Last name') }}" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>

                        @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="sex" class="col-md-1 col-form-label text-md-right"></label>

                    <div class="col-md-10">
                        <select id="sex" name="sex" class="form-control" required="required">
                            <option value="" disabled selected>Select Gender</option>
                            <option value="Female">Female</option>
                            <option value="Male">Male</option>
                          </select>
                        @error('sex')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="age" class="col-md-1 col-form-label text-md-right"></label>

                    <div class="col-md-10">
                        <input id="age" type="text" class="form-control @error('age') is-invalid @enderror" name="age" placeholder="{{ __('Age') }}" value="{{ old('age') }}" required autocomplete="last_name" autofocus>

                        @error('age')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="institution" class="col-md-1 col-form-label text-md-right"></label>

                    <div class="col-md-10">
                        <input id="institution" type="text" class="form-control @error('institution') is-invalid @enderror" placeholder="{{ __('Agency/Organization/Institution') }}" name="institution" value="{{ old('institution') }}" required autocomplete="institution" autofocus>

                        @error('institution')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="designation" class="col-md-1 col-form-label text-md-right"></label>

                    <div class="col-md-10">
                        <input id="designation" type="text" class="form-control @error('designation') is-invalid @enderror" name="designation" placeholder="{{ __('Designation') }}" value="{{ old('designation') }}" required autocomplete="institution" autofocus>

                        @error('designation')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="contact" class="col-md-1 col-form-label text-md-right"></label>

                    <div class="col-md-10">
                        <input id="contact" type="text" class="form-control @error('contact') is-invalid @enderror" name="contact" placeholder="{{ __('Contact No.') }}" value="{{ old('contact') }}" required autocomplete="institution" autofocus>

                        @error('contact')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="sectoral_affiliation" class="col-md-1 col-form-label text-md-right"></label>

                    <div class="col-md-10">
                        <select id="sectoral_affiliation" name="sectoral_affiliation" class="form-control" required="required">
                            <option value="" disabled selected>Sectorial Affilation</option>
                            <option value="PWD">PWD</option>
                            <option value="IP">IP</option>
                            <option value="NONE">NONE</option>
                          </select>
                        @error('sectoral_affiliation')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="remail" class="col-md-1 col-form-label text-md-right"></label>

                    <div class="col-md-10">
                        <input id="remail" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="{{ __('E-Mail Address') }}" value="{{ old('email') }}" required autocomplete="email">

                        @error('remail')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-1 col-form-label text-md-right"></label>

                    <div class="col-md-10">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="{{ __('Password') }}" required autocomplete="new-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password-confirm" class="col-md-1 col-form-label text-md-right"></label>

                    <div class="col-md-10">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="{{ __('Confirm Password') }}" required autocomplete="new-password">
                    </div>
                </div>

                <div class="form-group row mb-0" >
                    <div class="col-md-10 offset-md-1" >
                        <button type="submit" class="btn btn-primary form-control" style="background-color:black;">
                            {{ __('Register') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  
</div>

    <footer class="site-footer">
		<div class="container">
		  <div class="row">
			<div class="col-sm-12 col-md-6">
            <p class="u-text u-text-1" style="text-align:left;">
           
            <img src="img/nstw.png" class="img-rounded" alt="Cinque Terre" width="350" height="125">
   
              </p>


			 			</div>
	
			<div class="col-sm-12 col-md-3" style="text-align:left;">
			
                <font style="font-size: 1.125rem;">DEPARTMENT OF SCIENCE AND TECHNOLOGY</font></b>
                <br><b>
                  <font style="font-size: 1.125rem;">REGIONAL OFFICE NO. XI</font></b>
                <br>Cor. Friendship and Dumanlas , Bajada,&nbsp;Davao City, Philippines&nbsp;<br><br>
			</div>
	
			<div class="col-xs-6 col-md-3">
			 
              <ul class="footer-links"style="text-align:left;margin-left:28%;">
                  
                
              <a href="https://facebook.com/dostdavaoregion"><img src="img/2b.png" alt="Cinque Terre" width="15" height="15">  <font style="font-size: 14px; font-style: opensans;">&nbsp;&nbsp;@dostregion11</font> </a>
                <br><a href=" https://facebook.com/dostdavaoregion"></a><a href=" https://facebook.com/dostdavaoregion" class="fa fa-facebook" > &nbsp;&nbsp;<font style="font-size: 16px;font-style: opensans;"><strong>&nbsp;@dostregion11</strong></font></a>
                <br><a href=" https://twitter.com/DOSTRegion11"></a><a href=" https://twitter.com/DOSTRegion11" class="fa fa-twitter" style="font-size:16px; font-style:opensans;">&nbsp;&nbsp;<strong>@DOSTRegion11 </strong></a>
                <br><a href="https://www.instagram.com/dostregion11/"></a><a href="https://www.instagram.com/dostregion11/" class="fa fa-instagram" style="font-size:16px;font-style:opensans;">&nbsp;&nbsp;<strong> @dostregion11</strong> </a>
                
			  </ul>
			</div>
		  </div>
		  <hr>
		</div>
		<div class="container">
		  <div class="row">
			<div class="col-md-8 col-sm-6 col-xs-12" style="text-align:left;">
                <font style="font-size: 0.75rem;">© 2020 National Science and Technology Week (NSTW)&nbsp;Davao Region&nbsp;</font>
            
			</div>
	
			
		  </div>
		</div>
	  </footer>
</body>
</html>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
var APP_URL = "{{ url('/') }}";
$(document).ready(function() {
    // alert(APP_URL);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#frm_register").on("submit", function() {
        var formData = new FormData($(this)[0]);
            // formData.append('_method','post')
            $.ajax({
                type: "POST",
                url: APP_URL + '/register',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if(data.err!=undefined)
                    {
                        let err="";
                        $.each(data.err, function(data_err, msg) {
                        // console.log(msg);
                        err += "<li>"+msg[0]+"</li>";
                        });

                        $("#reg_error").addClass("alert alert-danger");
                        $("#reg_error ul").html(err);
                    }else if(data.msg!=undefined)
                    {
                        $("#first_name").val("");
                        $("#middle_name").val("");
                        $("#last_name").val("");
                        $("#sex").val("");
                        $("#age").val("");
                        $("#institution").val("");
                        $("#designation").val("");
                        $("#contact").val("");
                        $("#sectoral_affiliation").val("");
                        $("#email").val("");
                        $("#password").val("");
                        $("#password-confirm").val("");
                        swal(data.msg);
                    }
                },
                error: function (data)
                {
                    console.log('Error:', data);
                }
            });
            return false;
    });

    $("#frm_login").on("submit", function() {
        var formData = new FormData($(this)[0]);
            // formData.append('_method','post')
            $.ajax({
                type: "POST",
                url: APP_URL + '/login',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if(data.e!=undefined)
                    {
                        // let err="";
                        // $.each(data.e, function(data_err, msg) {
                        // console.log(msg);
                        // err += "<li>"+msg[0]+"</li>";
                        // });

                        // $("#login_error").addClass("alert alert-danger");
                        $("#login_error ul").html("<li>"+data.e+"</li>");
                        $("#login_error ul li").css("color", "red");
                    }else if(data.s!=undefined)
                    {
                        window.location.replace(APP_URL+"/home");
                    }
                },
                error: function (data)
                {
                    console.log('Error:', data);
                }
            });
            return false;
    });
});
</script>