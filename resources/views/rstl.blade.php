@extends('layouts.home')
@section('content')
<link href="/css/exhibit.css" rel="stylesheet">

<div class="container text-center border">
    <div class="hello_world " id="images">
    <a href="setup#apo" > <img id="" class="img-fluid" alt="Setup"   src="img/icons/setup_icon.jpg" ></a>
        <a href="rstl#apo" ><img id="" class="img-fluid" alt="RSTL"  src="img/icons/rstl_icon.jpg" ></a>
        <a href="cest#apo" ><img id="" class="img-fluid" alt="cest"  src="img/icons/cest_icon.jpg" ></a>
        <a href="innovation#apo" ><img id="" class="img-fluid" alt="Innovation"  src="img/icons/innovation_icon.jpg" ></a>
        <a href="schoolar#apo" ><img id="" class="img-fluid" alt="Scholarship"   src="img/icons/scholars_icon.jpg" ></a>

    </div>
</div>

  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
  
    <ol class="carousel-indicators" id="">
      
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src=""  style="width:100%;">

        <script>
            function thumbChange(num){
                var thumb = 'img/vid/cest/back' + num + '.png';
                document.getElementById("mainImg").src = thumb;
            }

        </script>
        <div  id="apo" class="container" >

            <div class="main" style="background-color:black; color: white;">
            <iframe width="540" height="315" src="https://www.youtube.com/embed/watch?v=v2VFPFFeb6I&list=PLl6a_Y8anztk53QGl5cFigXvzdfzS6FyD" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            
                <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
                    <div class="u-container-layout u-container-layout-2">
                        <br><br>
                      <h2 class="text1" style="font-size: 2vw;">
                   RDOST Regional Standards and Testing Laboratories Davao (RSTL Davao)
                    <br>
                      </h2>
                      <p class="text">
                        <font style="font-size: .9vw;"> is an internationally recognized laboratory that consistently delivers high quality testing and calibration services across the Davao Region, the Mindanao and the country. RSTL Davao has continuously expanded its its scope of services, invested  onto state of the art facilities, and increased workforce competencies to address the growing testing and calibration laboratory needs of the industry, the academe, the regulatory bodies, and private individuals as they meet in meeting their regulatory, statutory and customer requirements.
                         </font>
                        <span id="dots">...</span><span id="more">
                        <font  style="font-size: .9vw;">FIn 2005, RSTL Davao was first issued with international accreditation in PNS ISO/IEC 17025:2000 Laboratory Quality Management System by then called Bureau of Product Standards Laboratory Accreditation Scheme then named Phil. Accreditation Office (PAO). The RSTL Davao has sustained its accreditation to the international Laboratory Quality Management standard. With the 2017 version of the standard, RSTL Davao was the first government laboratory in the country who made a full transition and was audited by the Philippine Accreditation Bureau (PAB). Finally, in 2020, RSTL Davao was granted accreditation under the PNS ISO/IEC 17025:2017 specifically parameters and scopes in the   Chemical Testing Laboratory, Halal Verification Laboratory, Microbiological Testing Laboratory, and the Regional Metrology Laboratory.
                        Demonstrating compliance to the global requirements for competence, impartiality and consistent operation of laboratories, RSTL Davao also became a bonafide member of OneLab®, a network of accredited government and private laboratories in the Philippines and some parts of the world. OneLab® operates as a  referral system of laboratory activities using an IT-based solution to provide customers global access to comprehensive analytical and calibration services at a single touch point. The OneLab® network has provided RSTL Davao’s customers a faster, easier, and reliable laboratory technology. In fact, RSTL Davao’s services have steadily increased throughout the years since OneLab® was established in 2016.

                        </font>  </span>
                          <br><br>
                          <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore">Read More</h5>
                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                          <script type="text/javascript">
                            $(document).ready(function(){
                              $('#readMore').click(function(){
                                if($(this).html() == 'Read More'){
                                  $('#dots').hide();
                                  $('#more').show();
                                  $(this).html('Read Less');
  
                                }else {
                                  $('#dots').show();
                                  $('#more').hide();
                                  $(this).html('Read More');
                                }
                                });
                            });
                          </script>
                      </p>
                    </div>
                  </div>
            </div>
            <ul class="thumbs">
              
                
               

            </ul>

        </div>




      </div>

      

       
    </div>
    

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
@endsection



