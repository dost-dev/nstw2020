@extends('layouts.home')
@section('content')
<link href="/css/kinabukasan.css" rel="stylesheet">

<div class="container text-center border">
    <div class="hello_world " id="images">
    <a href="setup#apo" > <img id="" class="img-fluid" alt="Setup"   src="img/icons/setup_icon.jpg" ></a>
        <a href="rstl#apo" ><img id="" class="img-fluid" alt="RSTL"  src="img/icons/rstl_icon.jpg" ></a>
        <a href="cest#apo" ><img id="" class="img-fluid" alt="cest"  src="img/icons/cest_icon.jpg" ></a>
        <a href="innovation#apo" ><img id="" class="img-fluid" alt="Innovation"  src="img/icons/innovation_icon.jpg" ></a>
        <a href="schoolar#apo" ><img id="" class="img-fluid" alt="Scholarship"   src="img/icons/scholars_icon.jpg" ></a>
    </div>
    </div>

  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
  
    <ol class="carousel-indicators" id="">
      
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src=""  style="width:100%;">

        <script>
            function thumbChange(num){
                var thumb = 'img/vid/cest/back' + num + '.png';
                document.getElementById("mainImg").src = thumb;
            }

        </script>
       <div  id="apo" class="container" >

<div class="main" style="background-color:black; color: white;">
    
   <br><br>
   <iframe width="540" height="315" src="https://www.youtube.com/embed/0959pjho4iU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <div class="u-container-style u-layout-cell u-right-cell u-size-40 u-layout-cell-2">
        <div class="u-container-layout u-container-layout-2">
            
        <h2 class="text1" style="font-size: 2vw;">Scholarship


            </h2>
            <p class="text">
              <font style="font-size: .9vw;">The Department of Science and Technology (DOST) thru the Science Education Institute (SEI) is now accepting applicants for the 2020 S&T Undergraduate Scholarship program. This is open to Grade 12 students in AY 2019-2020 who are interested in pursuing Science and Technology degree programs in college.
                The S&T Undergraduate Scholarships Program aims to stimulate and entice talented Filipino youths to pursue lifetime productive careers in science and technology and ensure a steady, adequate supply of qualified S&T human resources which can steer the country towards national progress.

            <span id="dots">...</span><span id="more">
              <font  style="font-size: .9vw;">    The DOST Scholarship benefits include 40,000 tuition subsidy per academic year, 7,000 monthly stipend, and a 10,000 annual book allowance, among other benefits. Interested applicants can check the complete list of DOST’s S&T priority programs and study placements and can download the application forms at the DOST-SEI website – www. sei.dost.gov.ph. They can also obtain hard copies of the forms at the DOST Regional Office No. XI at cor. Friendship and Dumanlas Roads, Bajada, Davao City and to your nearest Provincial Science and Technology Centers (PSTCs) in Tagum City, Mati City, Digos City, Nabunturan and Malita.

                Deadline of submission of application forms and other requirements is on September 06, 2019 while the nationwide examination is set on October 20, 2019. For further inquiries, you may contact the Scholarship section of DOST XI through the following: (082) 227-1313 loc. 107 or dostxischolars@gmail.com.      </font>
  
              </font>  </span>
              <br><br>
              <h5 class="u-text u-text-default u-text-white u-text-3" href="javascript:void;" id="readMore">Read More</h5>
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
              <script type="text/javascript">
                $(document).ready(function(){
                  $('#readMore').click(function(){
                    if($(this).html() == 'Read More'){
                      $('#dots').hide();
                      $('#more').show();
                      $(this).html('Read Less');

                    }else {
                      $('#dots').show();
                      $('#more').hide();
                      $(this).html('Read More');
                    }
                    });
                });
              </script>
          </p>
        </div>
      </div>
</div>


</div>

                


</div>
      

       
    </div>
    

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
@endsection



