@extends('layouts.home')
@section('content')
<link href="/css/webinar.css" rel="stylesheet">
<style>

.responsive {
  width: 49%;
  height: auto;
}
</style>
<div class="container">
     <!-- 21:9 aspect ratio -->
     <h1 class="fon"><strong>2020 NSTW DAVAO FORA</strong></h1>
 <div class="upper">
  <a href="https://zoom.us/j/95468810047?pwd=a1B5aGh3cHl2dFpqVmNrYWpRUUFpQT09"><img src="img/webinar/PEDRO_speaker.jpg" alt="Nature" class="responsive" width="49%" height="400"></a>
  <a href="https://zoom.us/j/93402452392?pwd=RDQwVUNiMDh4Z0tPSlhpL2d2QVp2QT09"><img src="img/webinar/makerspace.jpg"class="responsive" width="49%" height="400"></a>
     
</div>
<div class="center">

<a href=" https://zoom.us/j/98415942393?pwd=Q3I0SkdJdEVORWJMMkVlbWFmKzVMUT09
    Passcode: 310849"><img src="img/webinar/PEDRO.jpg" class="responsive" width="49%" height="400"></a>
    <a href="https://zoom.us/j/93083132730?pwd=UUg5c2V0TnJOc2FXZ2ZIMzVhWG5xdz09"> <img src="img/webinar/SARAI.jpg"  class="responsive" width="49%" height="400"></a>


</div>

<div class="lower">

<a href="https://zoom.us/j/93083132730?pwd=UUg5c2V0TnJOc2FXZ2ZIMzVhWG5xdz09"><img src="img/webinar/AMCEN_speaker.jpg" class="responsive" width="49%" height="400"></a>
<a href="https://www.youtube.com/watch?v=PJMlM8O8ncA&feature=youtu.be&fbclid=IwAR2o9oG2ceLlUrBw2HDPAr57dXUGmQZDbpNTHUclpZWC5eErSdWMEx6Or4Q"><img src="img/webinar/CDC.jpg" alt="Nature" class="responsive" width="49%" height="400"></a>
  

</div>
<!-- 16:9 aspect ratio -->
</div>

@endsection