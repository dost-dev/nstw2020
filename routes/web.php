<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index')->name('index');
Route::get('/home', 'PageController@home')->name('home');


Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'middleware'=>'auth'
], function() {
    Route::get('/launching', 'PageController@launching')->name('launching');
    Route::get('/webinar', 'PageController@webinar')->name('webinar');
    Route::get('/exhibit', 'PageController@exhibit')->name('exhibit');
    Route::get('/kaayusan', 'PageController@kaayusan')->name('kaayusan');
    Route::get('/kabuhayan', 'PageController@kabuhayan')->name('kabuhayan');
    Route::get('/kalusugan', 'PageController@kalusugan')->name('kalusugan');
    Route::get('/kinabukasan', 'PageController@kinabukasan')->name('kinabukasan');
    Route::get('/cest', 'PageController@cest')->name('cest');
    Route::get('/rstl', 'PageController@rstl')->name('rstl');
    Route::get('/innovation', 'PageController@innovation')->name('innovation');
    Route::get('/schoolar', 'PageController@schoolar')->name('schoolar');
    Route::get('/setup', 'PageController@setup')->name('setup');
});